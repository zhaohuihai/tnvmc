
###################################### MPICH + Intel MKL ######################################
# specify boost header file directory
#BOOST_DIR :=

# setup for blas/lapack
#BLAS_LAPACK_INC := -I/opt/intel/mkl/include/
#BLAS_LAPACK_LIB := -L/opt/intel/mkl/lib/ -lmkl_intel_lp64 -lmkl_core -lpthread -lm -lmkl_intel_thread -lifcore -limf -lsvml -lintlc
#BLAS_LAPACK_LIB := -L/opt/intel/mkl/lib/ -lmkl_intel_lp64 -lmkl_core -lpthread -lm -lmkl_intel_thread -liomp5
#BLAS_LAPACK_LIB := -L${MKLROOT}/lib -Wl,-rpath,${MKLROOT}/lib -lmkl_intel_lp64 -lmkl_core -lmkl_intel_thread -lpthread -lm -ldl

#INTEL_INC := -I/opt/intel/include/
#INTEL_LIB := -L/opt/intel/lib -Wl,-rpath,/opt/intel/lib -liomp5

# scalapack
#scalib = $//Users/zhaohuihai/libraries/scalapack/lib/libscalapack.a
#scalib = 

# set compiler
# mpi c++ compiler
#MPICC = mpicxx
# c++ compiler
#CC = icpc
# fortran compiler
#FORT = ifort

#LIB = $(BLAS_LAPACK_LIB)
# set compilation flags
#CC_FLAGS := -O2 -no-prec-div -qopenmp -Wno-unknown-pragmas
#FFLAGS = -O3 -implicitnone
#CC_FLAGS_sfmt = -O3 -no-ansi-alias -xSSE2

#REPORT = -openmp-report1
#REPORT = 

#OPTION =
#OPTION_sfmt = -DMEXP=19937 -DHAVE_SSE2

# specify include directories
#GLOBAL_INC := -I./ $(BLAS_LAPACK_INC) -I$(BOOST_DIR) $(INTEL_INC)

#
#GLOBAL_LIB := $(LIB) $(INTEL_LIB)

###################################### ISSP system B (Sekirei) ######################################
## specify boost header file directory
BOOST_DIR :=
#
## setup for blas/lapack
#BLAS_LAPACK_INC := -I$(MKLROOT)/include/
BLAS_LAPACK_LIB := -L$(MKLROOT)/lib/intel64 -lmkl_intel_lp64 -lmkl_core -liomp5 -lpthread -lm -lmkl_intel_thread -lifcore

#CPP_INC := /home/app/gcc/4.8.5/include/
#
## scalapack
##scalib = $//Users/zhaohuihai/libraries/scalapack/lib/libscalapack.a
scalib = 
#
## set compiler
## mpi c++ compiler
MPICC = mpicxx
## c++ compiler
CC = icpc
## fortran compiler
FORT = ifort
#
LIB = 
## set compilation flags
CC_FLAGS := -O3 -no-prec-div -qopenmp -parallel -Wno-unknown-pragmas
FFLAGS = -O3 -implicitnone
CC_FLAGS_sfmt = -O3 -no-ansi-alias
#
##REPORT = -openmp-report1
REPORT = 
#
OPTION = -D_mpi_use
OPTION_sfmt = -DMEXP=19937 -DHAVE_SSE2
#
## specify include directories
GLOBAL_INC := -I./ $(BLAS_LAPACK_INC) -I$(BOOST_DIR) -I$(CPP_INC)
#
##
GLOBAL_LIB := $(LIB) $(BLAS_LAPACK_LIB)

###################################### K computer ######################################

## specify boost header file directory
#BOOST_DIR := 
#
## set compiler
## mpi c++ compiler
#MPICC = mpiFCCpx
## c++ compiler
#CC = FCCpx
## fortran compiler
# FORT = frtpx

#LIB = -SSL2BLAMP
## set compilation flags
#CC_FLAGS := -Kfast,parallel,ocl,openmp
#FFLAGS = -Kfast,ocl,auto,optmsg=2 -AT
#CC_FLAGS_sfmt = -Kfast,ocl,nomemalias
#
#REPORT = -Koptmsg=2
#
#OPTION =
#OPTION_sfmt = -DMEXP=19937
#
## specify include directories
#GLOBAL_INC := -I./ 
#
##
#GLOBAL_LIB := $(LIB)
