/*
 * time.h
 *
 *  Created on: 2015-3-22
 *      Author: zhaohuihai
 */

#ifndef SRC_TIME_H_
#define SRC_TIME_H_

void OutputTime(int step);
void InitTimer();

//inline void StartTimer(int n);
//inline void StopTimer(int n);

void StartTimer(int n);
void StopTimer(int n);

void OutputTimerParaOpt();
void OutputTimerPhysCal();

//void StartTimer(int n) {
//#ifdef _mpi_use
//  TimerStart[n]=MPI_Wtime();
//#else
//  struct timespec ts;
//  clock_gettime(CLOCK_REALTIME,&ts);
//  TimerStart[n]=ts.tv_sec + ts.tv_nsec*1.0e-9;
//#endif
//  return;
//}
//
//void StopTimer(int n) {
//#ifdef _mpi_use
//  Timer[n] += MPI_Wtime() - TimerStart[n];
//#else
//  struct timespec ts;
//  clock_gettime(CLOCK_REALTIME,&ts);
//  Timer[n] += ts.tv_sec + ts.tv_nsec*1.0e-9 - TimerStart[n];
//#endif
//  return;
//}


#endif /* SRC_TIME_H_ */
