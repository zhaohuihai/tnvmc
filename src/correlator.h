/*
 * correlator.h
 *
 *  Created on: 2016-1-15
 *      Author: zhaohuihai
 */

#ifndef SRC_CORRELATOR_H_
#define SRC_CORRELATOR_H_

#include <iostream>
#include <vector>

class Correlator {
private:
	bool _ti ;
	int _siteDim ; // dim of physical index
	int _nelements ;
	std::vector<int> _site ; // sites defining the correlator
	int _nsites ;            // number of sites in the lattice
	std::vector<int> _ti_sites ; // if a translated image, the sites defining the image's parent
	double* _elem ; // storage for the correlator's values

	int getPhysInd(const int* eleNum, const int i) ;
public:
	// default constructor
	Correlator() : _ti(false), _siteDim(0), _nelements(0), _nsites(0), _elem() {}
	// constructor from a vector of sites
	Correlator(const int nsites, const std::vector<int>& site, const int siteDim, double* elem) ;
	// translationally invariant constructor
	Correlator(const Correlator& other, const std::vector<int>& sites) ;

	bool ti() const {return _ti; } // return whether this is a translated image of another correlator
	int siteDim() const {return _siteDim; }
	int nelements() const { return _nelements; }
	int size() const { return _site.size() ; }
	std::vector<int> site() const { return _site; }
	std::vector<int> ti_sites() const {
		if (_ti) return _ti_sites;
		else {
			std::cout << "ti_sites() error: this correlator is not a translated image." << std::endl ;
			exit(0) ;
		}
		return _ti_sites ;
	}
	double* elem() const { return _elem; }
	double compute_weight(const int* eleNum) ;
	double compute_weight(const std::vector<int>& eleNum) ;
	// derivatives
	void derivative(double *der, const int* eleNum) ;
	void derivative(double &der_val, int &der_ind, const int* eleNum) ;
	void rescale(double maxElem) ;
	void info() ;
	void disp() ;
};



#endif /* SRC_CORRELATOR_H_ */
