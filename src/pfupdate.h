/*
 * pfupdate.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_PFUPDATE_H_
#define SRC_PFUPDATE_H_

void CalculateNewPfM(const int mi, const int s, double *pfMNew, const int *eleIdx,
                     const int qpStart, const int qpEnd);
void CalculateNewPfM2(const int mi, const int s, double *pfMNew, const int *eleIdx,
                     const int qpStart, const int qpEnd);
void UpdateMAll(const int mi, const int s, const int *eleIdx,
                const int qpStart, const int qpEnd);
void updateMAll_child(const int ma, const int s, const int *eleIdx,
                      const int qpStart, const int qpEnd, const int qpidx,
                      double *vec1, double *vec2);

double update_norm1(const int ma, const int s, const int *eleIdx,
		const int qpStart, const int qpEnd, const int qpidx) ;

std::vector<double> compute_invM_exact(const int *eleIdx,
		const int qpStart, const int qpEnd, const int qpidx) ;

#endif /* SRC_PFUPDATE_H_ */
