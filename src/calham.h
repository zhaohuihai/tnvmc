/*
 * calham.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_CALHAM_H_
#define SRC_CALHAM_H_

#include "correlator_product_state.h"
#include "fat_tree_tensor_network.h"

double CalculateHamiltonian(const double ip, int *eleIdx, const int *eleCfg,
       int *eleNum, const int *eleProjCnt);
double CalculateHamiltonian(const double ip, int *eleIdx, const int *eleCfg,
       int *eleNum, const int *eleProjCnt,
			 CorrelatorProductState& cps);
double CalculateHamiltonian(const double ip, int *eleIdx, const int *eleCfg,
       int *eleNum, const int *eleProjCnt,
			 FatTreeTensorNetwork& fttn);
double CalculateHamiltonian_sym(const double weight, int *eleIdx, const int *eleCfg,
       int *eleNum, FatTreeTensorNetwork& fttn);
double CalculateHamiltonian_symCorFactor(const double weight, int *eleIdx, const int *eleCfg,
       int *eleNum, FatTreeTensorNetwork& fttn);

double CalculateHamiltonian0(const int *eleNum);
double CalculateHamiltonian1(const double ip, int *eleIdx, const int *eleCfg,
                             int *eleNum, const int *eleProjCnt);
double CalculateHamiltonian2(const double ip, int *eleIdx, const int *eleCfg,
                             int *eleNum, const int *eleProjCnt);

#endif /* SRC_CALHAM_H_ */
