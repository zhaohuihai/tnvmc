/*
 * average.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_AVERAGE_H_
#define SRC_AVERAGE_H_

void WeightAverageWE(MPI_Comm comm);
void WeightAverageLanczos(MPI_Comm comm) ;
void WeightAverageSROpt(MPI_Comm comm);

void computeAve(MPI_Comm comm, int n, double *data, int nsample) ;
void computeAveSD(MPI_Comm comm, int n, double *data, double *data_SD, int nsample) ;

void WeightAverageGreenFunc(MPI_Comm comm);

void weightAverageReduce(int n, double *vec, MPI_Comm comm);

#endif /* SRC_AVERAGE_H_ */
