/*-------------------------------------------------------------
 * Variational Monte Carlo
 * fast Pfaffian update
 *-------------------------------------------------------------
 * by Satoshi Morita
 *-------------------------------------------------------------*/

#include "vmcmain.h"

#include "workspace.h"

#include "pfupdate.h"
#include "matrix.h"

using namespace std ;

/* Calculate new pfaffian. The ma-th electron with spin s hops. */
void CalculateNewPfM(const int ma, const int s, double *pfMNew, const int *eleIdx,
                     const int qpStart, const int qpEnd) {
  #pragma procedure serial
  const int qpNum = qpEnd-qpStart;
  const int msa = ma+s*Ne;
  const int rsa = eleIdx[msa] + s*Nsite;

  int qpidx;
  int msj,rsj;
  const double *sltE_a; /* update elements of msa-th row */
  const double *invM_a;
  double ratio;

  /* optimization for Kei */
  const int nsize = Nsize;
  const int ne = Ne;

  #pragma loop noalias
  for(qpidx=0;qpidx<qpNum;qpidx++) {
  	if ( McondNum[qpidx] > condNum_thresh ) { // bad condition
  		pfMNew[qpidx] = calculatePfaffian(eleIdx, qpStart, qpEnd, qpidx) ;
  	} else {
      sltE_a = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2 + rsa*Nsite2;
      invM_a = InvM + qpidx*Nsize*Nsize + msa*Nsize;

      ratio = 0.0;
      for(msj=0;msj<ne;msj++) {
        rsj = eleIdx[msj];
        ratio += invM_a[msj] * sltE_a[rsj];
      }
      for(msj=ne;msj<nsize;msj++) {
        rsj = eleIdx[msj] + Nsite;
        ratio += invM_a[msj] * sltE_a[rsj];
      }
      pfMNew[qpidx] = -ratio*PfM[qpidx];
  	}
  }
  return;
}

/* thread parallel version of CalculateNewPfM */
void CalculateNewPfM2(const int ma, const int s, double *pfMNew, const int *eleIdx,
                     const int qpStart, const int qpEnd) {
  const int qpNum = qpEnd-qpStart;
  const int msa = ma+s*Ne;
  const int rsa = eleIdx[msa] + s*Nsite;

  int qpidx;
  int msj,rsj;
  const double *sltE_a; /* update elements of msa-th row */
  const double *invM_a;
  double ratio;

  /* optimization for Kei */
  const int nsize = Nsize;
  const int ne = Ne;

  #pragma omp parallel for default(shared)        \
    private(qpidx,msj,sltE_a,invM_a,ratio,rsj)
  #pragma loop noalias
  for(qpidx=0;qpidx<qpNum;qpidx++) {
  	if ( McondNum[qpidx] > condNum_thresh ) { // bad condition
  		pfMNew[qpidx] = calculatePfaffian(eleIdx, qpStart, qpEnd, qpidx) ;
  	} else {
      sltE_a = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2 + rsa*Nsite2;
      invM_a = InvM + qpidx*Nsize*Nsize + msa*Nsize; // a column

      ratio = 0.0;
      for(msj=0;msj<ne;msj++) {
        rsj = eleIdx[msj];
        ratio += invM_a[msj] * sltE_a[rsj];
      }
      for(msj=ne;msj<nsize;msj++) {
        rsj = eleIdx[msj] + Nsite;
        ratio += invM_a[msj] * sltE_a[rsj];
      }
      // pf(B) = pf(A) * sum{j}_[invM(j,a)*slt(j,a)]
      //       = pf(A) * sum{j}_[-invM(a,j)*slt(j,a)]
      pfMNew[qpidx] = -ratio*PfM[qpidx];
  	}
  }
  return;
}

/* Update PfM and InvM. The ma-th electron with spin s hops to site ra=eleIdx[msi] */
void UpdateMAll(const int ma, const int s, const int *eleIdx,
                const int qpStart, const int qpEnd) {
  const int qpNum = qpEnd-qpStart;
  int qpidx;
  double *vec1,*vec2;
  int nRecal = 0 ;
  RequestWorkSpaceThreadDouble(2*Nsize);

  #pragma omp parallel default(shared) private(vec1,vec2)
  {
    vec1 = GetWorkSpaceThreadDouble(Nsize);
    vec2 = GetWorkSpaceThreadDouble(Nsize);

    #pragma omp for private(qpidx)
    #pragma loop nounroll
    for(qpidx=0;qpidx<qpNum;qpidx++) {
    	if ( McondNum[qpidx] > condNum_thresh ) { // bad condition
    		nRecal ++ ;
//    		fprintf(stderr, "warning: calculate Pfaffian by Pfapack. Condition number = %e\n", McondNum[qpidx]) ;
    		vector<int> myIWork(Nsize) ;
    		vector<double> myBufM(Nsize*Nsize) ;
    		vector<double> myWork(LapackLWork) ;
    		calculateMAll_child(eleIdx, qpStart, qpEnd, qpidx,
    		                    &(myBufM[0]), &(myIWork[0]), &(myWork[0]), LapackLWork);
    	} else {
    		updateMAll_child(ma, s, eleIdx, qpStart, qpEnd, qpidx, vec1, vec2);
    	}
    }

  }
  double recalRatio = (double)nRecal / (double)qpNum ;
  if ( recalRatio > 0.05 ) {
  	fprintf(stderr, "warning: calculate Pfaffian by Pfapack proportion: %e\n", recalRatio) ;
  }

  ReleaseWorkSpaceThreadDouble();
  return;
}

void updateMAll_child(const int ma, const int s, const int *eleIdx,
                      const int qpStart, const int qpEnd, const int qpidx,
                      double *vec1, double *vec2) {
  #pragma procedure serial
  /* const int qpNum = qpEnd-qpStart; */
  const int msa = ma+s*Ne;
  const int rsa = eleIdx[msa] + s*Nsite;
  const int nsize = Nsize; /* optimization for Kei */

  int msi,msj,rsi, rsj;

  const double *sltE_a; /* update elements of msa-th row */
  double sltE_aj;
  double *invM;
  double *invM_i,*invM_j,*invM_a;

  double vec1_i,vec2_i;
  double invVec1_a;
  double tmp;

  sltE_a = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2 + rsa*Nsite2; // sltE[rsa][:]
  const double *sltE = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2;
  const double* sltE_i;

  invM = InvM + qpidx*Nsize*Nsize;
  invM_a = invM + msa*Nsize;

  for(msi=0;msi<nsize;msi++) vec1[msi] = 0.0;

  /* Calculate vec1[i] = sum_j invM[i][j] sltE[a][j] */
  /* Note that invM[i][j] = -invM[j][i] */
  #pragma loop noalias
  for(msj=0;msj<nsize;msj++) {
    rsj = eleIdx[msj] + (msj/Ne)*Nsite;
    sltE_aj = sltE_a[rsj];
    invM_j = invM + msj*Nsize;

    for(msi=0;msi<nsize;msi++) {
      vec1[msi] += -invM_j[msi] * sltE_aj;
    }
  }
  //------------------------------------------------------
  double M_norm1 = update_norm1(ma, s, eleIdx, qpStart, qpEnd, qpidx) ;
  //------------------------------------------------------

  /* Update Pfaffian */
  /* Calculate -1.0/bufV_a to reduce division */
  tmp = vec1[msa];
  PfM[qpidx] *= -tmp;
  invVec1_a = -1.0/tmp; // = 1 / sum{m}_[invM[a][rsj]*sltE[a][rsj]]

  /* Calculate vec2[i] = -InvM[a][i]/vec1[a] */
  #pragma loop noalias
  for(msi=0;msi<nsize;msi++) {
    vec2[msi] = invM_a[msi] * invVec1_a;
  }

  /* Update InvM */
  #pragma loop noalias
  for(msi=0;msi<nsize;msi++) {
    invM_i = invM + msi*Nsize;
    vec1_i = vec1[msi];
    vec2_i = vec2[msi];
    for(msj=0;msj<nsize;msj++) {
      invM_i[msj] += vec1_i * vec2[msj] - vec1[msj] * vec2_i;
    }
    invM_i[msa] -= vec2_i; // msj = msa
  }

  #pragma loop noalias
  for(msj=0;msj<nsize;msj++) {
    invM_a[msj] += vec2[msj]; // msi = msa
  }
  /* end of update invM */
  // compute 1-norm condition number
  double Minv_norm1 = compute_norm1(invM, Nsize) ;
  McondNum[qpidx] = M_norm1 * Minv_norm1 ;
  return;
}

double update_norm1(const int ma, const int s, const int *eleIdx,
		const int qpStart, const int qpEnd, const int qpidx) {
	double* col_sum = Mcol_sum + (qpidx+qpStart) * Nsize ;
  // update M column-major
  // M[msj][msi] = - sltE[rsi][rsj]
  double *Mqp = M + (qpidx+qpStart) * Nsize*Nsize;
  double *Mqp_i ; // i-th column
  // update msa row
  int rsi, rsj ;
  const double *sltE = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2;
  const double* sltE_i;

  const int msa = ma+s*Ne;
  const int rsa = eleIdx[msa] + s*Nsite;
  const double *sltE_a = sltE + rsa * Nsite2 ;
  for ( int msi=0; msi<Nsize; msi++ ) {
  	rsi = eleIdx[msi] + (msi/Ne)*Nsite;
  	Mqp_i = Mqp + msi*Nsize ; // Mqp[:][msi]
  	sltE_i = sltE + rsi * Nsite2 ; // sltE[rsi][:]

  	col_sum[msi] -= fabs(Mqp_i[msa]) ;
  	Mqp_i[msa] = - sltE_i[rsa] ; // Mqp[msa][msi] = - sltE[rsi][rsa]
  	col_sum[msi] += fabs(Mqp_i[msa]) ;
  }
  // update msa column
  double *Mqp_a = Mqp + msa*Nsize ; // Mqp[:][msa]
  for ( int msj = 0; msj < Nsize; msj ++ ) {
  	rsj = eleIdx[msj] + (msj/Ne)*Nsite ;
  	Mqp_a[msj] = -sltE_a[rsj] ; // Mqp[msj][msa] = - sltE[rsa][rsj]
  }
  col_sum[msa] = xasum(Nsize, Mqp_a) ;

  double M_norm = 0.0 ;
  for ( int msi=0;msi<Nsize;msi++ ) {
  	M_norm = max(M_norm, col_sum[msi]) ;
  }
  return M_norm ;
}

vector<double> compute_invM_exact(const int *eleIdx,
		const int qpStart, const int qpEnd, const int qpidx) {
  int msi,msj;
  int rsi,rsj;
  const double *sltE = SlaterElm + (qpidx+qpStart)*Nsite2*Nsite2;
	vector<double> invM(Nsize*Nsize, 0.0) ;
	for(msi=0;msi<Nsize;msi++) {
		rsi = eleIdx[msi] + (msi/Ne)*Nsite;
		int iN = msi * Nsize ;
		int iN2 = rsi * Nsite2 ;
		for(msj=0;msj<Nsize;msj++) {
			rsj = eleIdx[msj] + (msj/Ne)*Nsite;
			invM[iN + msj] = -sltE[iN2 + rsj];
		}
	}

	int m=Nsize , n=Nsize, lda=Nsize;
	int info = 0 ;
	int lwork = LapackLWork ;
	vector<int> iwork(Nsize, 0.0) ;
	vector<double> work(LapackLWork, 0.0) ;

	dgetrf_(&m, &n, &(invM[0]), &lda, &(iwork[0]), &info); /* ipiv = iwork */
	if ( info != 0) {
		cout << "dgetrf is not converged." << endl ;
		cout << info << endl ;
	}
	dgetri_(&n, &(invM[0]), &lda, &(iwork[0]), &(work[0]), &lwork, &info);
	if ( info != 0 ) {
		cout << "dgetri is not converged." << endl ;
		cout << info << endl ;
	}
  for(msi=0;msi<Nsize;msi++) {
  	int iN = msi * Nsize ;
    for(msj=0;msj<Nsize;msj++) {
    	// bufM_i2[i + j * Nsize]
    	// bufM_i[j + i * Nsize]
    	// skew symmetrize invM
      invM[iN + msj] = 0.5*(invM[msi + msj * Nsize] - invM[iN + msj]);
      /* invM[i][j] = 0.5*(bufM[i][j]-bufM[j][i]) */
    }
  }
  return invM ;
}
