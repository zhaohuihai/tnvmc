/*
 * blas_lapack.hpp
 *
 *  Created on: 2015-7-13
 *      Author: zhaohuihai
 */

#ifndef BLAS_LAPACK_HPP_
#define BLAS_LAPACK_HPP_

#include <vector>
//#ifdef HAVE_MKL
//	#include <mkl.h>
//#endif

extern "C" {
// BLAS
double dasum_(const int *n, const double* x, const int* incx) ;
void dcopy_(const int *n, const double *x, const int *incx, double *y, const int *incy);
void dscal_(const int *n, const double *a, double *x, const int *incx) ;
void daxpy_(const int *n, const double *a, const double *x, const int *incx, double *y, const int *incy) ;
double ddot_(const int* n, const double *x, const int* incx, const double *y, const int* incy) ;
void dgemv_(char *trans, const int *m, const int *n, const double *alpha, const double *a, int *lda,
						const double *x, int *incx, const double *beta, double *y, int *incy) ;
double dnrm2_(int *n, double *x, int *incx) ;
int idamax_(int *n, double *dx, int *incx);
void dgemm_(char *transa, char *transb, int *m, int *n, int *k, double *alpha,
		        double *a, int *lda, double *b, int *ldb, double *beta, double *c, int *ldc);

// LAPACK
void dsyevr_(char *jobz, char *range, char *uplo, int *n, double *a, int *lda, double *vl, double *vu,
		         int *il, int *iu, double *abstol, int *m, double *w, double *z, int *ldz,
						 int *isuppz, double *work, int *lwork, int *iwork, int *liwork, int *info) ;
void dgeev_(char *jobvl, char *jobvr, int *n, double *a, int *lda, double *wr, double *wi,
		        double *vl, int *ldvl, double *vr, int *ldvr, double *work, int *lwork, int *info);
void dgesdd_(char *jobz, int *m, int *n, double *a, int *lda, double *s, double *u, int *ldu,
		         double *vt, int *ldvt, double *work, int *lwork, int *iwork, int *info);
void dgesvd_(char *jobu, char *jobvt, int *m, int *n, double *a, int *lda, double *s,
		         double *u, int *ldu, double *vt, int *ldvt, double *work, int *lwork, int *info);
void dgeqrf_(int *m, int *n, double *a, int *lda, double *tau, double *work, int *lwork, int *info);
void dgelqf_(int *m, int *n, double *a, int *lda, double *tau, double *work, int *lwork, int *info);
void dgetri_(int *n, double *a, int *lda, int *ipiv, double *work, int *lwork, int *info);
void dgetrf_(int *m, int *n, double *a, int *lda, int *ipiv, int *info);
}

//============================== BLAS ==============================

//
inline double xasum(const int n, const double *x) {
	int incx = 1 ;
	return dasum_(&n, x, &incx) ;
}

// y = x
inline void xcopy(const int n, const double *x, double *y)
{
	int incx = 1 ;
	int incy = 1 ;
	dcopy_(&n, x, &incx, y, &incy) ;
}

inline void xcopy(const int n, const std::vector<double>& x, std::vector<double>& y)
{
	int incx = 1 ;
	int incy = 1 ;
	dcopy_(&n, &(x[0]), &incx, &(y[0]), &incy) ;
}

// x = a*x
inline void xscal(const int n, const double a, double *x)
{
	int incx = 1 ;
	dscal_(&n, &a, x, &incx) ;
}

inline void xscal(const int n, const double a, std::vector<double>& x) {
	int incx = 1 ;
	dscal_(&n, &a, &(x[0]), &incx) ;
}

// vector addition. y = a*x + y
inline void xaxpy(const int n, const double a, const double *x, const int incx,
									double *y, const int incy)
{
	daxpy_(&n, &a, x, &incx, y, &incy) ;
}
inline void xaxpy(const int n, const double a, const double *x, double *y)
{
	int incx = 1 ;
	int incy = 1 ;
	daxpy_(&n, &a, x, &incx, y, &incy) ;
}

// res = sum{i}_[x(i)*y(i)]
inline double xdot(const int n, const double *x, const double *y)
{
	int incx = 1 ;
	int incy = 1 ;
	return ddot_(&n, x, &incx, y, &incy) ;
}

// y = x + y
//inline void xaxpby(const int n, const std::vector<double>& x, std::vector<double>& y)
//{
//	int incx = 1 ;
//	int incy = 1 ;
//	double a = 1.0 ;
//	double b = 1.0 ;
//	daxpby(&n, &a, &(x[0]), &incx, &b, &(y[0]), &incy) ;
//}

// Computes a matrix-vector product using a general matrix
// y := alpha*A*x + beta*y or y := alpha*A'*x + beta*y
inline void xgemv(char trans, const int m, const int n,
									const double alpha, const double *a, const double *x,
									const double beta, double *y)
{
	int lda = m ;
	int incx = 1 ;
	int incy = 1 ;
	dgemv_(&trans, &m, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy) ;
}
// y := a*x or y := a'*x
inline void xgemv(char trans, const int m, const int n, const double *a, const double *x, double *y)
{
	int lda = m ;
	int incx = 1 ;
	int incy = 1 ;
	double alpha = 1.0 ;
	double beta = 0.0 ;
	dgemv_(&trans, &m, &n, &alpha, a, &lda, x, &incx, &beta, y, &incy) ;
}

inline void xgemv(char trans, const int m, const int n,
									const std::vector<double>& a, const std::vector<double>& x, std::vector<double>& y)
{
	int lda = m ;
	int incx = 1 ;
	int incy = 1 ;
	double alpha = 1.0 ;
	double beta = 0.0 ;
	dgemv_(&trans, &m, &n, &alpha, &(a[0]), &lda, &(x[0]), &incx, &beta, &(y[0]), &incy) ;
}

//============================== LAPACK ==============================

inline int xsyevr(int n, double* a, double* w, double* z, int* isuppz, double* work, int* iwork)
{
	char jobz = 'V' ; // eigenvalues and eigenvectors are computed.
	char range = 'A' ; // the routine computes all eigenvalues.
	char uplo = 'U' ; // a stores the upper triangular part of A.

	int lda = n ;

	double vl = 0.0 ;
	double vu = 0.0 ;

	int il = 0 ;
	int iu = 0 ;

	double abstol = 0.0 ; // The absolute error tolerance to which each eigenvalue/eigenvector is required.

	int m = n ;
	int ldz = n ;

	int lwork = 26 * n ;
	int liwork = 10 * n ;
	int info = 0 ;

	dsyevr_(&jobz, &range, &uplo, &n, a, &lda, &vl, &vu, &il, &iu, &abstol, &m,
			   w, z, &ldz, isuppz, work, &lwork, iwork, &liwork, &info) ;

	return info ;
}

inline int xsyevr(int n, std::vector<double>& a, std::vector<double>& w, std::vector<double>& z,
		              std::vector<int>& isuppz, std::vector<double>& work, std::vector<int>& iwork)
{
	char jobz = 'V' ; // eigenvalues and eigenvectors are computed.
	char range = 'A' ; // the routine computes all eigenvalues.
	char uplo = 'U' ; // a stores the upper triangular part of A.

	int lda = n ;

	double vl = 0.0 ;
	double vu = 0.0 ;

	int il = 0 ;
	int iu = 0 ;

	double abstol = 0.0 ; // The absolute error tolerance to which each eigenvalue/eigenvector is required.

	int m = n ;
	int ldz = n ;

	int lwork = work.size() ;
	int liwork = iwork.size() ;
	int info ;

	dsyevr_(&jobz, &range, &uplo, &n, &(a[0]), &lda, &vl, &vu, &il, &iu, &abstol, &m,
			   &(w[0]), &(z[0]), &ldz, &(isuppz[0]), &(work[0]), &lwork, &(iwork[0]), &liwork, &info) ;

	return info ;
}

#endif /* BLAS_LAPACK_HPP_ */
