/*
 * locgrn.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_LOCGRN_H_
#define SRC_LOCGRN_H_

#include "correlator_product_state.h"
#include "fat_tree_tensor_network.h"

double GreenFunc1(const int ri, const int rj, const int s, const double ip,
                  int *eleIdx, const int *eleCfg, int *eleNum, const int *eleProjCnt,
                  int *projCntNew, double *buffer);
double GreenFunc1(const int ri, const int rj, const int s, const double ip,
    int *eleIdx, const int *eleCfg, int *eleNum, const int *eleProjCnt,
    int *projCntNew, double *buffer,
		CorrelatorProductState& cps);
double GreenFunc1(const int ri, const int rj, const int s, const double ip,
    int *eleIdx, const int *eleCfg, int *eleNum, const int *eleProjCnt,
    int *projCntNew, double *buffer,
		FatTreeTensorNetwork& fttn);
double GreenFunc1_sym(const int ri, const int rj, const int s,
		const double weight, int *eleIdx, const int *eleCfg, int *eleNum,
		double *buffer, FatTreeTensorNetwork& fttn);
double GreenFunc1_symCorFactor(const int ri, const int rj, const int s,
		const double weight, int *eleIdx, const int *eleCfg, int *eleNum,
		double *buffer, FatTreeTensorNetwork& fttn);

double GreenFunc2(const int ri, const int rj, const int rk, const int rl,
                  const int s, const int t, const double ip,
                  int *eleIdx, const int *eleCfg, int *eleNum, const int *eleProjCnt,
                  int *projCntNew, double *buffer);
double GreenFunc2(const int ri, const int rj, const int rk, const int rl,
    const int s, const int t, const double ip,
    int *eleIdx, const int *eleCfg, int *eleNum, const int *eleProjCnt,
    int *projCntNew, double *buffer,
		CorrelatorProductState& cps);
double GreenFunc2(const int ri, const int rj, const int rk, const int rl,
    const int s, const int t, const double ip,
    int *eleIdx, const int *eleCfg, int *eleNum, const int *eleProjCnt,
    int *projCntNew, double *buffer,
		FatTreeTensorNetwork& fttn);
double GreenFunc2_sym(const int ri, const int rj, const int rk, const int rl,
    const int s, const int t, const double weight,
    int *eleIdx, const int *eleCfg, int *eleNum, double *buffer,
		FatTreeTensorNetwork& fttn);

double GreenFuncN(const int n, int *rsi, int *rsj, const double ip,
                  int *eleIdx, const int *eleCfg, int *eleNum, const int *eleProjCnt,
                  double *buffer, int *bufferInt);
double GreenFuncN(const int n, int *rsi, int *rsj, const double ip,
                  int *eleIdx, const int *eleCfg, int *eleNum, const int *eleProjCnt,
                  double *buffer, int *bufferInt,FatTreeTensorNetwork& fttn);

double calculateNewPfMN_child(const int qpidx, const int n, const std::vector<int>& msa,
															const int *rsa, const int *eleIdx, double *buffer);


#endif /* SRC_LOCGRN_H_ */
