/*
 * vmcmake.h
 *
 *  Created on: 2015-3-22
 *      Author: zhaohuihai
 */

#ifndef SRC_VMCMAKE_H_
#define SRC_VMCMAKE_H_

#include "correlator_product_state.h"
#include "fat_tree_tensor_network.h"

void VMCMakeSample(MPI_Comm comm);
void VMCMakeSample(MPI_Comm comm, CorrelatorProductState& peps) ;
void VMCMakeSample(MPI_Comm comm_parent, MPI_Comm comm, FatTreeTensorNetwork& fttn) ;

int makeInitialSample(int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt,
                      const int qpStart, const int qpEnd, MPI_Comm comm);
void copyFromBurnSample(int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt);
void copyToBurnSample(const int *eleIdx, const int *eleCfg, const int *eleNum, const int *eleProjCnt);
void saveEleConfig(const int sample, const double logIp,
                   const int *eleIdx, const int *eleCfg, const int *eleNum, const int *eleProjCnt);
void sortEleConfig(int *eleIdx, int *eleCfg, const int *eleNum);

void ReduceCounter(MPI_Comm comm);

//================================ new function ================================

void makeCandidate_hopping(int *mi_, int *ri_, int *rj_, int *s_, int *rejectFlag_,
                           const int *eleIdx, const int *eleCfg);
void makeCandidate_exchange(int *mi_, int *ri_, int *rj_, int *s_, int *rejectFlag_,
                            const int *eleIdx, const int *eleCfg, const int *eleNum);
void updateEleConfig(int mi, int ri, int rj, int s,
                     int *eleIdx, int *eleCfg, int *eleNum);
void revertEleConfig(int mi, int ri, int rj, int s,
                     int *eleIdx, int *eleCfg, int *eleNum);

typedef enum {HOPPING, EXCHANGE, NONE} UpdateType;
UpdateType getUpdateType(int path);

double find_array_max(const double* a, const int len) ;
double find_array_min(const double* a, const int len, int& minIdx) ;

void replaceBadConditionSample(int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt,
														   const int qpStart, const int qpEnd, MPI_Comm comm) ;

#endif /* SRC_VMCMAKE_H_ */


