/*
 * parameter.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_PARAMETER_H_
#define SRC_PARAMETER_H_

#include "correlator_product_state.h"
#include "fat_tree_tensor_network.h"

#define D_AmpMax             1

void InitParameter();
int ReadInitParameter(char *initFile);
int ReadInitParameter(char *initFile, FatTreeTensorNetwork& fttn);

int findBondDim(int NfileTNelem) ;

void SyncModifiedParameter(MPI_Comm comm);
void SyncModifiedParameter(MPI_Comm comm, CorrelatorProductState& cps) ;

void SyncModifiedParameter(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;

void mixOptSteps(const int Optstep) ;

void rescaleSROpt_max(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;
void rescaleSROpt_max_CG(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;
void rescaleSROpt_max_noCG(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;

void rescaleSROpt_mean(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;
void rescaleSROpt_mean_CG(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;
void rescaleSROpt_mean_noCG(MPI_Comm comm, FatTreeTensorNetwork& fttn) ;

double findMax(int n, std::vector<double> V, int i0) ;
double findMean(int n, std::vector<double> V, int i0) ;

void SetFlagShift();

void shiftGJ();
double shiftDH2();
double shiftDH4();



#endif /* SRC_PARAMETER_H_ */

//======================================================================================
