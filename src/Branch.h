/*
 * Branch.h
 *
 *  Created on: 2016年12月20日
 *      Author: zhaohuihai
 */

#ifndef SRC_BRANCH_H_
#define SRC_BRANCH_H_

#include "blas_lapack.hpp"

class Branch {
private:
	std::vector<double> _vec ;
	double logCoef ; // _vec * exp(logCoef)

public:
	// default constructor
	Branch() : logCoef(0.0) {}
	// constructor
	Branch(double* first, double* last) ;

	void normalize() ;
};

#endif /* SRC_BRANCH_H_ */
