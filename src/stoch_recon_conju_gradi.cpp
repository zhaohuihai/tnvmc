/*
 * stoch_recon_conju_gradi.cpp
 *
 *  Created on: 2015-6-28
 *      Author: zhaohuihai
 */

#include <vector>
#include <fstream>
#include "vmcmain.h"
#include "sfmt/SFMT.h"
#include "workspace.h"
#include "vmctime.h"
#include "safempi.h"
#include "blas_lapack.hpp"

#include "stoch_recon_conju_gradi.h"

using namespace std ;

StochReconConjuGradi::StochReconConjuGradi(MPI_Comm comm,
		double stepWidth, double lowBound, int& info)
{
	_stepWidth = stepWidth ;
	int pi ;

	info = 0 ;
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);
	vector<double> sDiagElm(NPara, 0.0);
	vector<double> sDiagElm_SD(NPara, 0.0) ;
	// S(i,i) = OOdiag[i] - O[i] * O[i]
	for (pi = 0; pi < NPara; pi ++) {
		sDiagElm[pi] = SROptOOdiag[pi + 1] - SROptO[pi + 1] * SROptO[pi + 1] ;
		sDiagElm_SD[pi] = computeSdiag_SD(comm, SROptOOOOdiag[pi + 1], SROptOOOdiag[pi + 1], SROptOOdiag[pi + 1], SROptO[pi + 1]) ;
	}
	if ( rank == 0 && output_Smat ) {
		cout << "output S diagonal elements" << endl ;
		output_Sdiag(sDiagElm, sDiagElm_SD) ;
		MPI_Abort(comm, 1) ;
	}

	double sDiagMax,sDiagMin;
	findMaxMin(NPara, sDiagElm, sDiagMax, sDiagMin) ;
	double diagCutThreshold = sDiagMax * DSROptRedCut;

//	int paraToSmatIdx[NPara], smatToParaIdx[NPara];
	vector<int> paraToSmatIdx(NPara, 0) ;
	vector<int> smatToParaIdx(NPara, 0) ;

	int optNum=0,cutNum=0, errCutNum = 0;
	int si = 0 ;
	for (pi = 0; pi < NPara; pi ++) {
		if (OptFlag[pi]!=1) { // fixed by OptFlag
			paraToSmatIdx[pi] = -1;
      optNum++; // num of para not optimized
      continue;
		}

		if ( sDiagElm[pi] <= diagCutThreshold ) { // fixed by diagCut
			paraToSmatIdx[pi] = -1 ;
			cutNum++;
		} else if ( (sDiagElm_SD[pi] / sDiagElm[pi]) > DSROptErrCut ) {
			// truncate the parameter with relative error larger than DSROptErrCut
			paraToSmatIdx[pi] = -1 ;
			cutNum++;
			errCutNum ++ ;
		} else { // optimized
      paraToSmatIdx[pi] = si; // pi: Para index
      smatToParaIdx[si] = pi; // si: S matrix index
      si += 1;
		}
	}
	_nSmat = si ; //
	if ( _nSmat == 0 ) return ;
	for (si = _nSmat; si < NPara; si ++) {
		smatToParaIdx[si] = -1;
	}
	initialize(smatToParaIdx, sDiagElm, sDiagElm_SD, stepWidth) ;

	findMaxMin(_nSmat, _sDiag, sDiagMax, sDiagMin) ;
	// uniform shift
	if (sDiagMax > 0) _diag_shift = sDiagMax * DSROptStaDel ;
	else _diag_shift = DSROptStaDel ;

	if ( CGprecond == 1 ) {
		preconditioned_conjugate_gradient_restart(_nSmat,
										 max_cg_iter,
										 &_x_vec.at(0),
										 &_b_vec.at(0),
										 &_y_vec.at(0),
										 &_d_vec.at(0),
										 &_r_vec.at(0),
										 comm) ;
	} else {
	  conjugate_gradient_restart(_nSmat,
											 max_cg_iter,
											 &_x_vec.at(0),
											 &_b_vec.at(0),
											 &_y_vec.at(0),
											 &_d_vec.at(0),
											 &_r_vec.at(0),
											 comm) ;
	}

  FILE *fp;
  double rmax;
  int simax;
  /*** print zqp_SRinfo.dat ***/
  rmax = _x_vec[0]; simax=0;
  for(si=0;si<_nSmat;si++) {
    if(fabs(rmax) < fabs(_x_vec[si])) {
      rmax = _x_vec[si]; simax=si;
    }
  }
  if ( rank == 0 ) {
  	if (errCutNum > 0)  cout << "errCutNum: " << errCutNum << endl ;
    fprintf(FileSRinfo, "%5d %5d %5d %5d % .5e % .5e % .5e %5d\n",
    		NPara,_nSmat,optNum,cutNum,sDiagMax,sDiagMin,rmax,smatToParaIdx[simax]);

    /*** check inf and nan ***/
    for(si=0;si<_nSmat;si++) {
      if( !isfinite(_x_vec[si]) ) {
        fprintf(stderr, "StcOptCG: r[%d]=%.10lf\n",si,_x_vec[si]);
        info = 1;
        break;
      }
    }
  }
	double randCoef ;
	/* update variatonal parameters */
	for (si = 0; si < _nSmat; si++) {
//		cout << si << ": " << _x_vec[si] << endl ;
		pi = smatToParaIdx[si];
		randCoef = genrand_real2() * (1 - lowBound) + lowBound ;

		ParaChange[pi] = _x_vec[si] ;
		Para[pi] += randCoef * _x_vec[si] ;
	}
}

void StochReconConjuGradi::initialize(std::vector<int>& smatToParaIdx,
		std::vector<double>& sDiagElm, std::vector<double>& sDiagElm_SD, const double stepWidth)
{
	_Os.assign(size_t(_nSmat * NVMCSample), 0.0) ; // O(k,i)
	_O.assign(size_t(_nSmat), 0.0) ; // <O(k)>
	_sDiag.assign(size_t(_nSmat), 0.0) ; //
	_sDiag_shift.assign(size_t(_nSmat), 0.0) ;

	_x_vec.assign(size_t(_nSmat), 0.0) ; // solution vector
	_b_vec.assign(size_t(_nSmat), 0.0) ; // b vector
	_y_vec.assign(size_t(_nSmat), 0.0) ;
	_d_vec.assign(size_t(_nSmat), 0.0) ;
	_r_vec.assign(size_t(_nSmat), 0.0) ;

	_OOd.assign(size_t(_nSmat), 0.0) ;
	_process_y.assign(size_t(_nSmat), 0.0) ;

	_Od.assign(size_t(NVMCSample), 0.0) ;
	//======================================================
	int sIdx, pIdx ;
	int offset ;
	int si ;
	int pi ;

	for (int i = 0; i < NVMCSample; i ++) {
		offset = i * SROptSize ;
		for (si = 0; si < _nSmat; si ++) {
			pi = smatToParaIdx[si] ;
			sIdx = si + i * _nSmat ;
			_Os[sIdx] = SROptO_Store[pi + 1 + offset] ;
		}
	}
	//********************************************
	// debug
//	Tensor<double> Oten(SROptO_Store, SROptSize, NVMCSample) ;
//	Oten.display() ;
//	Tensor<double> Osten(&_Os[0], _nSmat, NVMCSample) ;
//	Osten.display() ;
//	exit(0) ;
	//********************************************
	for (si = 0; si < _nSmat; si ++ ) {
		pi = smatToParaIdx[si] ;
		_O[si] = SROptO[pi + 1] ;
		_sDiag[si] = sDiagElm[pi] ;
//		_sDiag_shift[si] = sDiagElm_SD[pi] ;
		if ( sDiagElm[pi] < (sDiagElm_SD[pi] *  DSROptSftErr) ) {
			_sDiag_shift[si] = sDiagElm_SD[pi] *  DSROptSftErr - sDiagElm[pi] ;
		} else {
//			_sDiag_shift[si] = sDiagElm_SD[pi] ;
			_sDiag_shift[si] = 0 ;
		}
		// _b_vec energy gradient
		_b_vec[si] = -stepWidth * 2.0 * (SROptHO[pi+1] - SROptHO[0] * SROptO[pi+1]) ;
		if ( CGinitial == 1 ) _x_vec[si] = _b_vec[si] ;
		else if ( CGinitial == 2 ) _x_vec[si] = ParaChange[pi] ;
	}
	//*********************************************
}

double StochReconConjuGradi::computeSdiag_SD(MPI_Comm comm, double OOOO, double OOO, double OO, double O) {
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);
	double n = size * NVMCSample ;
	// varS = (1/n)(<OOOO> - 4*<OOO>*<O> + 8*<OO>*<O>^2 - 4 * <O>^4 - <OO>^2)
	double varS = (1.0/n) * (OOOO - 4 * OOO * O + 8 * OO * O * O - 4 * O * O * O * O - OO * OO) ;
	return sqrt(varS) ;
}

void StochReconConjuGradi::output_Sdiag() {
	string fileName = "S_diag.dat" ;
	ofstream outfile(&(fileName[0]), ios::out) ;
	if (!outfile) {
		cerr << "output_Sdiag: open error!" << endl ;
		exit(0) ;
	}
	outfile.precision(14) ;
	for ( int i = 0; i < _nSmat; i ++ ) {
		outfile << _sDiag[i] << endl ;
	}
}

void StochReconConjuGradi::output_Sdiag(vector<double>& sDiagElm, vector<double>& sDiagElm_SD) {
	string fileName = "S_diag.dat" ;
	ofstream outfile(&(fileName[0]), ios::out) ;
	if (!outfile) {
		cerr << "output_Sdiag: open error!" << endl ;
		exit(0) ;
	}
	outfile.precision(14) ;
	for ( int i = 0; i < NPara; i ++ ) {
		outfile << sDiagElm[i] << ",  " << sDiagElm_SD[i]  << endl ;
	}
}

void StochReconConjuGradi::findMaxMin(int n, std::vector<double>& sDiagElm, double& sDiagMax, double& sDiagMin)
{
	double sDiag = sDiagElm[0];
	sDiagMax = sDiag ;
	sDiagMin = sDiag;
	for ( int pi = 1; pi < n; pi ++ ) {
		sDiag = sDiagElm[pi];
		if(sDiag>sDiagMax) sDiagMax=sDiag;
		if(sDiag<sDiagMin) sDiagMin=sDiag;
	}
}

bool StochReconConjuGradi::conjugate_gradient(const int n, // dimension of the system
			const int m,       // maximum number of iterations
			double * const x , // size n. on entry, a guess for x. on exit, the solution vector x.
			double * const b , // size n.  on entry, the vector b.    on exit, contents are overwritten.
			double * const y , // size n.  vector used as workspace.  on exit, contents are overwritten.
			double * const d , // size n.  vector used as workspace.  on exit, contents are overwritten.
			double * const r , // size n.  vector used as workspace.  on exit, contents are overwritten.
			MPI_Comm comm)
{
  int myrank, nproc;
  MPI_Comm_rank(comm,&myrank);
  MPI_Comm_size(comm,&nproc);

  double delta = 100.0 ;
  double alpha, beta ;

  // initialize the residual as r = M*b - M*A*x
  operate_by_M(n, b, r, comm) ; // r = M * b
  operate_by_A(n, x, y, comm) ; // y = A * x
  operate_by_M(n, y, d, comm) ; // d = M * A * x
  // r = M*b - M*A*x
  xaxpy(n, -1.0, d, r) ; // r = -d + r
  //***********************************
  //***********************************
  // initialize the search direction as d = r
  xcopy(n, r, d) ;

  // use the b vector to hold v
  double * const v = b ;

  // initialize v to M^(-1) x
  operate_by_M_inv(n, x, v, comm) ;

  // compute the initial residual norm
  delta = xdot(n, r, r) ;
  double delta0 = delta ;
  // iteratively compute v
  int iter = 0 ;
  for ( ; iter < m; iter ++)
  {
  	// check if we are finished
  	const bool conv = converged(fabs(sqrt(delta)), fabs(sqrt(delta0)), comm) ;
  	if (conv) break ;

  	// compute y = (M A M) d       (use x as an intermediate)
  	operate_by_M(n, d, y, comm) ; // y = M * d || y = d
  	operate_by_A(n, y, x, comm) ; // x = A * y
  	operate_by_M(n, x, y, comm) ; // y = M * x || y = x

  	// compute alpha
  	alpha = delta / xdot(n, d, y);

  	// update v
  	xaxpy(n, alpha, d, v) ; // v = alpha*d + v
  	// update the residual
    xaxpy(n, -alpha, y, r) ; // r = r - alpha * y
  	// compute beta
  	beta = xdot(n, r, r) / delta ;

  	// compute new residual norm
  	delta = beta * delta ;

  	// compute new search direction
  	xscal(n, beta, d) ;
  	xaxpy(n, 1.0, r, d) ; // d = r + d
  }
  // check if the method converged
  const bool conv= converged(fabs(sqrt(delta)), fabs(sqrt(delta0)), comm) ;
  if (conv && myrank == 0)
  {
  	cout << "conjugate gradient converged in " <<  iter << " iterations." << endl << endl;
  }
  else if ( myrank == 0 )
  {
  	cout << "conjugate gradient did not converge after " << iter <<  " iterations." << endl << endl;
  }

  // we currently have the solution to the equation (M A M) (M^(-1) x) = (M b), so multiply by M to get solution to A x = b
  operate_by_M(n, v, x, comm); // x = M * v

  return conv ;
}
// without preconditioning
bool StochReconConjuGradi::conjugate_gradient_restart(const int n, 			 // dimension of the system
			const int m,       // maximum number of iterations
			double * const x , // size n. on entry, a guess for x. on exit, the solution vector x.
			double * const b , // size n.  on entry, the vector b.
			double * const q , // size n.  vector used as workspace.  on exit, contents are overwritten.
			double * const d , // size n.  vector used as workspace.  on exit, contents are overwritten.
			double * const r , // size n.  vector used as workspace.  on exit, contents are overwritten.
			MPI_Comm comm)
{
  int myrank, nproc;
  MPI_Comm_rank(comm,&myrank);
  MPI_Comm_size(comm,&nproc);

  double delta = 100.0 ;
  double alpha, beta ;
  vector<double> CGerr ;
  // initialize the residual as r = b - A*x
  operate_by_A(n, x, r, comm) ; // r = A * x
  xscal(n, -1.0, r) ; // r -> -r
  // r = b + r
  xaxpy(n, 1.0, b, r) ; // r = b - A*x
  //***********************************
  //***********************************
  // initialize the search direction as d = r
  xcopy(n, r, d) ;

  // compute the initial residual norm
  delta = xdot(n, r, r) ;
  double delta0 = _stepWidth * _stepWidth ;
  CGerr.push_back(sqrt(delta)) ;

  // iteratively compute x
  int iter = 0 ;
  for ( ; iter < m; iter ++) {
  	// check if we are finished
  	const bool conv = converged(fabs(sqrt(delta)), fabs(sqrt(delta0)), comm) ;
  	if (conv) break ;

  	// compute q = A d
  	operate_by_A(n, d, q, comm) ;
//  	if ( iter == 1 ) exit(0) ;
//  	if ( iter == 5 ) cout << "d[0]: " << d[0] << endl ;
  	// compute alpha
  	alpha = delta / xdot(n, d, q);

  	// update x
  	xaxpy(n, alpha, d, x) ; // x = alpha*d + x
  	//=====================================================
//  	if (myrank == 0) cout << iter << ",  " << x[0] << endl ;
  	//=====================================================
  	// update the residual
  	if (((iter+1) % 20) == 0) {
  		operate_by_A(n, x, r, comm) ; // r = A * x
  		xscal(n, -1.0, r) ; // r -> -r
  		xaxpy(n, 1.0, b, r) ; // r = b - A*x
  	}
  	else {
  		xaxpy(n, -alpha, q, r) ; // r = r - alpha * q
  	}
  	// compute beta
  	beta = xdot(n, r, r) / delta ;

  	// compute new residual norm
  	delta = beta * delta ;
  	CGerr.push_back(sqrt(delta)) ;
//  	if ( myrank == 0 ) std::cout << "delta: " << delta << std::endl ;
  	// compute new search direction
  	xscal(n, beta, d) ;
  	xaxpy(n, 1.0, r, d) ; // d = r + d
  }
//  if (myrank == 0) cout << x[0] << endl ;
  // check if the method converged
  const bool conv= converged(fabs(sqrt(delta)), fabs(sqrt(delta0)), comm) ;
  if (conv && myrank == 0) {
  	cout << "conjugate gradient converged in " <<  iter << " iterations." << endl ;
  }
  else if ( myrank == 0 ) {
  	cout << "conjugate gradient did not converge after " << iter <<  " iterations." << endl;
  	cout << "convergence error: " << sqrt(delta) << endl ;
  }

  if ( myrank == 0 && output_CGerr ) {
		cout << "output CG error" << endl ;
		output_CG_error(CGerr) ;
		MPI_Abort(comm, 1) ;
  }
  return conv ;
}

//-----------------------------------------------------------------------------------
// Solves a system of linear equations A x = b using the conjugate gradient method.
// Using preconditioner M, the equations become M^(-1) A x = M^(-1) b
// A diagonal elememts are shifted as A(i,i) + shift(i)
// M is diagonal matrix with diagonal elememts defined as A(i,i) + shift(i)
//-----------------------------------------------------------------------------------
bool StochReconConjuGradi::preconditioned_conjugate_gradient_restart(const int n, // dimension of the system
		const int m, // maximum number of iterations
		double * const x , // size n. on entry, a guess for x. on exit, the solution vector x.
		double * const b , // size n.  on entry, the vector b.    on exit, contents are overwritten.
		double * const q , // size n.  vector used as workspace.  on exit, contents are overwritten.
		double * const d , // size n.  vector used as workspace.  on exit, contents are overwritten.
		double * const r , // size n.  vector used as workspace.  on exit, contents are overwritten.
		MPI_Comm comm) {
  int myrank, nproc;
  MPI_Comm_rank(comm,&myrank);
  MPI_Comm_size(comm,&nproc);

  double delta_new, delta_old ;
  double alpha, beta ;
  vector<double> CGerr ;
  // initialize the residual as r = M^(-1)(b - A*x)
  operate_by_A(n, x, r, comm) ; // r = A * x
  xscal(n, -1.0, r) ; // r -> -r
  // r = 1.0*b + r
  xaxpy(n, 1.0, b, r) ; // r = b - A*x
  // initialize the search direction as d = M^(-1) * r
  operate_by_M_inv(n, r, d, comm) ;

  // compute the initial residual norm
  delta_new = xdot(n, r, d) ;
  double delta0 = _stepWidth * _stepWidth ; ;
  CGerr.push_back(sqrt(delta_new)) ;

  // iteratively compute x
  int iter = 0 ;
  for ( ; iter < m; iter ++ ) {
  	// check if we are finished
  	const bool conv = converged(fabs(sqrt(delta_new)), fabs(sqrt(delta0)), comm) ;
  	if ( conv ) break ;

  	// compute q = A d
  	operate_by_A(n, d, q, comm) ;
  	// compute alpha
  	alpha = delta_new / xdot(n, d, q) ;
  	// update x
  	xaxpy(n, alpha, d, x) ; // x = alpha*d + x
  	//=====================================================
//  	if (myrank == 0) cout << iter << ",  " << x[0] << endl ;
  	//=====================================================
  	// update the residual
  	if (((iter+1) % 20) == 0) {
  		operate_by_A(n, x, r, comm) ; // r = A * x
  		xscal(n, -1.0, r) ; // r -> -r
  		xaxpy(n, 1.0, b, r) ; // r = 1.0*b + r , r = b - A*x
  	} else {
  		xaxpy(n, -alpha, q, r) ; // r = - alpha * q + r
  	}
  	// q = M^(-1)*r
  	operate_by_M_inv(n, r, q, comm) ;

  	delta_old = delta_new ;
  	delta_new = xdot(n, r, q) ;
  	CGerr.push_back(sqrt(delta_new)) ;
  	// compute beta
  	beta = delta_new / delta_old ;

  	xscal(n, beta, d) ; // d = beta * d
  	xaxpy(n, 1.0, q, d) ; // d = 1.0*q + d
  }
  // check if the method converged
  const bool conv = converged(fabs(sqrt(delta_new)), fabs(sqrt(delta0)), comm) ;
  if (conv && myrank == 0) {
  	cout << "conjugate gradient converged in " <<  iter << " iterations." << endl ;
  }
  else if ( myrank == 0 ) {
  	cout << "conjugate gradient did not converge after " << iter <<  " iterations." << endl ;
  	cout << "convergence error: " << sqrt(delta_new) << endl ;
  }

  if ( myrank == 0 && output_CGerr ) {
		cout << "output CG error" << endl ;
		output_CG_error(CGerr) ;
		MPI_Abort(comm, 1) ;
  }

  return conv ;
}

void StochReconConjuGradi::output_CG_error(std::vector<double>& CGerr) {
	string fileName = "CG_error.dat" ;
	ofstream outfile(&(fileName[0]), ios::out) ;
	if (!outfile) {
		cerr << "output_Sdiag: open error!" << endl ;
		exit(0) ;
	}
	outfile.precision(14) ;
	for ( int i = 0; i < CGerr.size(); i ++ ) {
		outfile << CGerr[i]  << endl ;
	}
}

// function to perform y = A d  (A == S = <OO>-<O><O>)
void StochReconConjuGradi::operate_by_A(const int n,
										  double * const d, double * const y, MPI_Comm comm)
{
	int rank, nproc ;
	MPI_Comm_rank(comm, &rank) ;
  MPI_Comm_size(comm, &nproc);
	// broadcast the trial vector to all processes
//	if ( rank == 0 ) MPI_Bcast(d, n, MPI_DOUBLE, 0, comm) ;
	MPI_Bcast(d, n, MPI_DOUBLE, 0, comm) ;
	// zero the result vector
	xscal(n, 0.0, y) ;

	// compute action of each visited configuration's overlap matrix on the search direction
	xscal(n, 0.0, &(_process_y[0]) ) ;
	xscal(n, 0.0, &(_OOd[0]) ) ;
	xscal(NVMCSample, 0.0, &(_Od[0]) ) ;
	// Od(j) = sum{l}_[Os(l,j)*d(l)]
	xgemv('T', n, NVMCSample, &(_Os[0]), d, &(_Od[0]) ) ;
	//********************************************************
	// process_y(k) = sum{j}_[Os(k,j)*Od(j)]
	xgemv('N', n, NVMCSample, &(_Os[0]), &(_Od[0]), &(_process_y[0]) ) ;
	MPI_Barrier(comm) ;
	// reduce actions from all processes on the root process
	SafeMpiAllReduce(&(_process_y[0]), y, n, comm) ;
//	if (rank == 0)
//		for ( int k = 0; k < _nSmat; k ++ ) cout << k << ", " << y[k] << endl ;
	// y = y / (NVMCSample * nproc)
	double a = 1.0 / (double)(NVMCSample * nproc) ;
	xscal(n, a, y) ;
	//----------------------------------
	// coef = sum{l}_[<O(l)>*d(l)]
	double coef = xdot(n, &(_O[0]), d) ;
	coef = 0.0 - coef ;
	// y(k) = - coef * <O(k)> + y(k)
	xaxpy(n, coef, &(_O[0]), y) ;
	//-------------------------------------------------
	// add diagonal shift on root process
	if (rank == 0) {
		// y(k) = DSROptStaDel * d(k) + y(k)
		for (int k = 0; k < n; k ++) {
//			y[k] += DSROptStaDel * _sDiag[k] * d[k] ;
//			y[k] += _diag_shift * d[k] ;
			y[k] += (_diag_shift * d[k] + _sDiag_shift[k] * d[k]) ;
		}
	}
}

// function to perform d = M r, where M is preconditioning operator
void StochReconConjuGradi::operate_by_M(const int n,
											double * const r, double * const d, MPI_Comm comm)
{
	int myrank ;
	MPI_Comm_rank(comm, &myrank) ;
	if (myrank == 0) {
		xcopy(n, r, d) ; // d = r
	}
}
// function to perform d = M^(-1) r, where M is preconditioning operator
void StochReconConjuGradi::operate_by_M_inv(const int n,
		                      double * const r, double * const d, MPI_Comm comm)
{
	int myrank ;
	MPI_Comm_rank(comm, &myrank) ;
	if (myrank == 0) {
		for ( int i = 0 ; i < n; i ++ ) {
			d[i] = r[i] / (_sDiag[i] + _diag_shift + _sDiag_shift[i]) ;
		}
	}
}

// function to check if the cojugate gradient iterations have converged
bool StochReconConjuGradi::converged(const double residual, const double residual0, MPI_Comm comm)
{
	int myrank ;
	MPI_Comm_rank(comm, &myrank) ;
	if (myrank == 0) {
//		cout << "residual: " << residual << endl ;
		_cg_converged = (residual < (cg_thresh * residual0) ) ;
	}
//	MPI_Bcast(&_cg_converged, 1, MPI_C_BOOL, 0, comm) ;
	MPI_Bcast(&_cg_converged, 1, MPI::BOOL, 0, comm) ;
	return _cg_converged ;
}

bool StochReconConjuGradi::converged(const double residual, MPI_Comm comm) {
	cout << "converged(const double, MPI_Comm) not implemented yet." << endl ;
	exit(0) ;
}

// x = alpha*d + x. if relative change of x is smaller than cg_thresh, converged.
//bool StochReconConjuGradi::x_converged(double alpha, double * d, double * x, MPI_Comm comm) {
//	int myrank ;
//
//}

