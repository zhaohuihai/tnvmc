/*-------------------------------------------------------------
 * Variational Monte Carlo
 * calculate physical quantities
 *-------------------------------------------------------------
 * by Satoshi Morita and Ryui Kaneko
 *-------------------------------------------------------------*/

#include <string>
#include <fstream>
#include "vmcmain.h"

#include "splitloop.h"
#include "vmctime.h"
#include "matrix.h"
#include "qp.h"
#include "projection.h"
#include "calham.h"
#include "slater.h"
#include "calgrn.h"
#include "lslocgrn.h"
#include "average.h"

#include "vmccal.h"

using namespace std ;

void VMCMainCal(MPI_Comm comm, CorrelatorProductState& cps)
{
  int *eleIdx,*eleCfg,*eleNum,*eleProjCnt;
  double local_energy ;
  double x,w,ip;
  double we,sqrtw;

  const int qpStart=0;
  const int qpEnd=NQPFull;
  int sample, sampleStart, sampleEnd, sampleSize;
  int i,info;

  /* optimazation for Kei */
  const int nProj=NProj;
  double *srOptO = SROptO;
  int *srOptO_ind ;

  int rank, size, int_i;
  MPI_Comm_size(comm,&size);
  MPI_Comm_rank(comm,&rank);

  SplitLoop(&sampleStart,&sampleEnd,NVMCSample,rank,size);

  /* initialization */
  clearPhysQuantity();

  for(sample=sampleStart;sample<sampleEnd;sample++) {
    eleIdx = EleIdx + sample*Nsize;
    eleCfg = EleCfg + sample*Nsite2;
    eleNum = EleNum + sample*Nsite2;
    eleProjCnt = EleProjCnt + sample*NProj;
//-------------------------------------
    StartTimer(40);
    info = CalculateMAll(eleIdx,qpStart,qpEnd);
    StopTimer(40);

    if(info!=0) {
      fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d info:%d (CalculateMAll)\n",rank,sample,info);
      continue;
    }

    ip = CalculateIP(PfM,qpStart,qpEnd,MPI_COMM_SELF);
    cps.initialize(eleNum) ;
    x = LogProjVal(eleProjCnt);
    /* calculate reweight */
//    w = exp(2.0*(log(fabs(ip))+x) - logSqPfFullSlater[sample]);
    w = 1.0 ;
    if( !isfinite(w) ) {
      fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d w=%e\n",rank,sample,w);
      continue;
    }

    StartTimer(41);
    /* calculate energy */
    local_energy = CalculateHamiltonian(ip,eleIdx,eleCfg,eleNum,eleProjCnt, cps);
//    cout << "local_energy: " << local_energy << endl ;
//    exit(0) ;
    StopTimer(41);
    if( !isfinite(local_energy) ) {
      fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d e=%e\n",rank,sample,local_energy);
      continue;
    }

    Wc += w;
    Etot  += w * local_energy;
    Etot2 += w * local_energy * local_energy;
    if(NVMCCalMode==0) {
    	if (useSRCG) {
    		if ( isSparseO ) { // store non zero derivative and index
    			srOptO = SROptO_Store + sample * SROptSize_sparse ;
    			srOptO_ind = SROptO_ind + sample * SROptSize_sparse ;
    		} else {
    			srOptO = SROptO_Store + sample * SROptSize ;
    		}
    	}
      /* Calculate O for correlation factors */
    	srOptO[0] = 1.0;
      #pragma loop noalias
      for(i=0;i<nProj;i++) srOptO[i+1] = (double)(eleProjCnt[i]);

      StartTimer(42);
      /* SlaterElmDiff */
      SlaterElmDiff(srOptO+NProj+1, ip, eleIdx);
      StopTimer(42);
      if(FlagOptTrans>0) {
      	calculateOptTransDiff(srOptO+NProj+NSlater+1, ip);
      }

      int cps_offset = 1 + NSlater + NProj + NOptTrans ;
      if ( useSRCG && isSparseO ) {
        for ( int j = 0 ; j < cps_offset; j ++ ) srOptO_ind[j] = j ;
        cps.derivative(srOptO+cps_offset, srOptO_ind+cps_offset, cps_offset, eleNum) ;
//        for ( int j = 0 ; j < SROptSize_sparse; j ++ ) {
//        	cout << srOptO_ind[j] << ", " << srOptO[j] << endl ;
//        }
//        exit(0) ;
      } else {
      	cps.derivative(srOptO+cps_offset, eleNum) ;
      }

      StartTimer(43);
      /* Calculate OO and HO */
      if ( useSRCG ) {
      	if ( isSparseO ) {
      		// Calculate <O>
      		calculateO(SROptO, srOptO, srOptO_ind, SROptSize_sparse) ;
      		// Calculate <HO>
      		calculateHO(SROptHO, srOptO, srOptO_ind, w, local_energy, SROptSize_sparse) ;
      		// Calculate diagonal elements of <OO>
      		calculateOOdiag(SROptOOdiag, srOptO, srOptO_ind, SROptSize_sparse) ;
      	} else {
          // Calculate <O>
          calculateO(SROptO, srOptO, SROptSize) ;
          // Calculate <HO>
          calculateHO(SROptHO, srOptO, w, local_energy, SROptSize) ;
          // Calculate diagonal elements of <OO>
          calculateOOdiag(SROptOOdiag, srOptO, SROptSize) ;
      	}
      }
      else { // not use CG
        if (NStoreO==0) {
        	calculateOO(SROptOO,SROptHO,SROptO,w,local_energy,SROptSize);
        }
        else {
        	we    = w*local_energy;
        	sqrtw = sqrt(w);
  				#pragma omp parallel for default(shared) private(int_i)
        	for(int_i=0;int_i<SROptSize;int_i++)
        	{
        		// SROptO_Store for fortran
        		SROptO_Store[int_i+sample*SROptSize]  = sqrtw*SROptO[int_i];
        		SROptHO[int_i]                       += we*SROptO[int_i];
        	}
        }
      }

      StopTimer(43);

    } else if(NVMCCalMode==1) {
      StartTimer(42);
      /* Calculate Green Function */
      CalculateGreenFunc(w,ip,eleIdx,eleCfg,eleNum,eleProjCnt, cps);
      StopTimer(42);

      if(NLanczosMode>0){
        /* Calculate local QQQQ */
        StartTimer(43);
        LSLocalQ(local_energy,ip,eleIdx,eleCfg,eleNum,eleProjCnt);
        calculateQQQQ(QQQQ,LSLQ,w,NLSHam);
        StopTimer(43);
        if(NLanczosMode>1){
          /* Calculate local QcisAjsQ */
          StartTimer(44);
          LSLocalCisAjs(local_energy,ip,eleIdx,eleCfg,eleNum,eleProjCnt);
          calculateQCAQ(QCisAjsQ,LSLCisAjs,LSLQ,w,NLSHam,NCisAjs);
          calculateQCACAQ(QCisAjsCktAltQ,LSLCisAjs,w,NLSHam,NCisAjs,
                          NCisAjsCktAlt,CisAjsCktAltIdx);
          StopTimer(44);
        }
      }
    }
  } /* end of for(sample) */

//  for (int i = 0 ; i < SROptSize; i ++) {
//  	cout << i << ": " << SROptO[i] << setw(16) << ", " << SROptHO[i] << setw(16) << ", " << SROptOOdiag[i] << endl ;
//  }
//  exit(0) ;
  // calculate OO and HO at NVMCCalMode==0
  if(NStoreO!=0 && NVMCCalMode==0 && !useSRCG){
  	sampleSize=sampleEnd-sampleStart;
  	StartTimer(45);
  	calculateOO_Store(SROptOO,SROptHO,SROptO_Store,w,local_energy,SROptSize,sampleSize);
  	StopTimer(45);
  }
  return;
}

void VMCMainCal(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
  int *eleIdx,*eleCfg,*eleNum,*eleProjCnt;
  double local_energy ;
  double x,w,ip;
  double we,sqrtw;

  const int qpStart=0;
  const int qpEnd=NQPFull;
  int sample, sampleStart, sampleEnd, sampleSize;
  int i,info;

  /* optimazation for Kei */
  const int nProj=NProj;
  double *srOptO = SROptO;
  int *srOptO_ind ;

  int rank, size, int_i;
  MPI_Comm_size(comm,&size);
  MPI_Comm_rank(comm,&rank);

  SplitLoop(&sampleStart,&sampleEnd,NVMCSample,rank,size);

  /* initialization */
  clearPhysQuantity();

  std::vector<double> energy_sample ;
  energy_sample.clear() ;
  for(sample=sampleStart;sample<sampleEnd;sample++) {
    eleIdx = EleIdx + sample*Nsize;
    eleCfg = EleCfg + sample*Nsite2;
    eleNum = EleNum + sample*Nsite2;
    eleProjCnt = EleProjCnt + sample*NProj;
//-------------------------------------
    StartTimer(40);
    info = CalculateMAll(eleIdx,qpStart,qpEnd);
    StopTimer(40);

    if(info!=0) {
      fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d info:%d (CalculateMAll)\n",rank,sample,info);
      continue;
    }

    ip = CalculateIP(PfM,qpStart,qpEnd,MPI_COMM_SELF);
    fttn.initialize(eleNum) ;
//    fttn.clear_new_branches() ;
    x = LogProjVal(eleProjCnt);
    /* calculate reweight */
//    w = exp(2.0*(log(fabs(ip))+x) - logSqPfFullSlater[sample]);
    w = 1.0 ;
    if( !isfinite(w) ) {
      fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d w=%e\n",rank,sample,w);
      continue;
    }

    StartTimer(41);
    /* calculate energy */
    local_energy = CalculateHamiltonian(ip,eleIdx,eleCfg,eleNum,eleProjCnt, fttn);
    energy_sample.push_back(local_energy) ;
//    cout << "local_energy: " << local_energy << endl ;
//    exit(0) ;
    StopTimer(41);
    if( !isfinite(local_energy) ) {
      fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d e=%e\n",rank,sample,local_energy);
      continue;
    }

    Wc += w;
    Etot  += w * local_energy;
    Etot2 += w * local_energy * local_energy;
    if(NVMCCalMode==0) {
    	if (useSRCG) {
    		if ( isSparseO ) { // store non zero derivative and index
    			srOptO = SROptO_Store + sample * SROptSize_sparse ;
    			srOptO_ind = SROptO_ind + sample * SROptSize_sparse ;
    		} else {
    			srOptO = SROptO_Store + sample * SROptSize ;
    		}
    	}
    	StartTimer(42);
//    	 Calculate O for correlation factors
    	srOptO[0] = 1.0;
      #pragma loop noalias
      for(i=0;i<nProj;i++) srOptO[i+1] = (double)(eleProjCnt[i]);


      /* SlaterElmDiff */
      SlaterElmDiff(srOptO+NProj+1, ip, eleIdx);

      if(FlagOptTrans>0) {
      	calculateOptTransDiff(srOptO+NProj+NSlater+1, ip);
      }

      int fttn_offset = 1 + NProj + NSlater + NOptTrans ;
      fttn.derivative(srOptO+fttn_offset, eleNum) ;
      StopTimer(42);
      //***********************************************************
//      for (int i = 0; i < NPara; i ++) {
//      	if ( srOptO[i + 1] != 0 )
//      		cout << i << ": " << srOptO[i + 1] << endl ;
//      }
//      exit(0) ;
      //***********************************************************
      StartTimer(43);
      /* Calculate OO and HO */
      if ( useSRCG ) {
      	if ( isSparseO ) {
      		// Calculate <O>
      		calculateO(SROptO, srOptO, srOptO_ind, SROptSize_sparse) ;
      		// Calculate <HO>
      		calculateHO(SROptHO, srOptO, srOptO_ind, w, local_energy, SROptSize_sparse) ;
      		// Calculate diagonal elements of <OO>
      		calculateOOdiag(SROptOOdiag, srOptO, srOptO_ind, SROptSize_sparse) ;
      	} else {
          // Calculate <O>
          calculateO(SROptO, srOptO, SROptSize) ;

          // Calculate <HO>
          calculateHO(SROptHO, srOptO, w, local_energy, SROptSize) ;
          // Calculate diagonal elements of <OO>
          calculateOOdiag(SROptOOdiag, srOptO, SROptSize) ;
          // Calculate diagonal elements of <OOO>
          calculateOpowDiag(SROptOOOdiag, srOptO, 3, SROptSize) ;
          // Calculate diagonal elements of <OOOO>
          calculateOpowDiag(SROptOOOOdiag, srOptO, 4, SROptSize) ;

      	}
      }
      else { // not use CG
        if (NStoreO==0) {
        	calculateOO(SROptOO,SROptHO,SROptO,w,local_energy,SROptSize);
        }
        else {
        	we    = w*local_energy;
        	sqrtw = sqrt(w);
  				#pragma omp parallel for default(shared) private(int_i)
        	for(int_i=0;int_i<SROptSize;int_i++) {
        		// SROptO_Store for fortran
        		SROptO_Store[int_i+sample*SROptSize]  = sqrtw*SROptO[int_i];
        		SROptHO[int_i]                       += we*SROptO[int_i];
        	}
        }
      }
      StopTimer(43);
    } else if(NVMCCalMode==1) {
      StartTimer(42);
      /* Calculate Green Function */
      CalculateGreenFunc(w,ip,eleIdx,eleCfg,eleNum,eleProjCnt, fttn);
      StopTimer(42);
//
      if(NLanczosMode>0){
        /* Calculate local QQQQ */
        StartTimer(43);
        LSLocalQ(local_energy,ip,eleIdx,eleCfg,eleNum,eleProjCnt,fttn);
        calculateQQQQ(QQQQ,LSLQ,w,NLSHam);
        StopTimer(43);
        if(NLanczosMode>1){
          /* Calculate local QcisAjsQ */
          StartTimer(44);
          LSLocalCisAjs(local_energy,ip,eleIdx,eleCfg,eleNum,eleProjCnt,fttn);
          calculateQCAQ(QCisAjsQ,LSLCisAjs,LSLQ,w,NLSHam,NCisAjs);
          calculateQCACAQ(QCisAjsCktAltQ,LSLCisAjs,w,NLSHam,NCisAjs,
                          NCisAjsCktAlt,CisAjsCktAltIdx);
          StopTimer(44);
        }
      }
    }
  } /* end of for(sample) */
  //---------------------------------------------------------------------------------------
//  for (int i = 0; i < NPara; i ++) {
//  	if ( SROptO[i + 1] != 0 )
//  		cout << i << ": " << SROptO[i + 1] << endl ;
//  }
//  exit(0) ;
  //---------------------------------------------------------------------------------------
  if ( rank == 0 && output_energy ) {
  	output_energy_sample(energy_sample) ;
  }
  // calculate OO and HO at NVMCCalMode==0
  if(NStoreO!=0 && NVMCCalMode==0 && !useSRCG){
  	sampleSize=sampleEnd-sampleStart;
  	StartTimer(45);
  	calculateOO_Store(SROptOO,SROptHO,SROptO_Store,w,local_energy,SROptSize,sampleSize);
  	StopTimer(45);
  }
  return;
}

void VMCMainCal_sym(MPI_Comm comm, FatTreeTensorNetwork& fttn) {
	int *eleIdx,*eleCfg,*eleNum;
	double local_energy ;
	double w;
	double we,sqrtw;

	const int qpStart=0;
	const int qpEnd=NQPFull;
	int sample, sampleStart, sampleEnd, sampleSize;
	int info;

	/* optimazation for Kei */
	const int nProj=NProj;
	double *srOptO = SROptO;
	int *srOptO_ind ;

	int rank, size, int_i;
	MPI_Comm_size(comm,&size);
	MPI_Comm_rank(comm,&rank);

	SplitLoop(&sampleStart,&sampleEnd,NVMCSample,rank,size);

	/* initialization */
	clearPhysQuantity();

	for(sample=sampleStart;sample<sampleEnd;sample++) {
		eleIdx = EleIdx + sample*Nsize;
		eleCfg = EleCfg + sample*Nsite2;
		eleNum = EleNum + sample*Nsite2;
		//-------------------------------------
		StartTimer(40);
		info = CalculateMAll(eleIdx,qpStart,qpEnd);
		StopTimer(40);

		if(info!=0) {
			fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d info:%d (CalculateMAll)\n",rank,sample,info);
			continue;
		}

		double weight = CalculateWeight(PfM, eleNum, fttn, qpStart, qpEnd, MPI_COMM_SELF ) ;
//		fttn.initialize(eleNum) ;
		w = 1.0 ;

		StartTimer(41);
		/* calculate energy */
		local_energy = CalculateHamiltonian_sym(weight,eleIdx,eleCfg,eleNum, fttn);
		StopTimer(41);
		if( !isfinite(local_energy) ) {
			fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d e=%e\n",rank,sample,local_energy);
			continue;
		}

		Wc += w;
		Etot  += w * local_energy;
		Etot2 += w * local_energy * local_energy;

		srOptO = SROptO_Store + sample * SROptSize ;
		srOptO[0] = 1.0;
		//---------------------------------------------------------
		vector<double> PfDerQPfull ;
		computePfDerQPfull(PfDerQPfull, eleIdx) ; // [NSlater*NQPFull]

		vector<int> projCnt(NProj, 1.0) ;
	  vector<int> eleNum_shift(2*Nsite, -1) ;
		for ( int i = 0 ; i < NMPTrans; i ++ ) { // loop to create translational samples
			int * mqp = QPTrans[i] ;
			eleNum_shift = createSftEleNum(mqp, eleNum) ;
			// correlation factors
			MakeProjCnt(&(projCnt[0]), &(eleNum_shift[0])) ;
			double ProjWeight = ProjVal(&(projCnt[0])) ;
			// Pfaffian
			double PfWeight = 0.0 ;
			vector<double> PfDerMP(NSlater, 0.0) ;
			for ( int s = 0; s < NSPGaussLeg; s ++ ) { // loop for spin projection
				int qpidx = s + i * NSPGaussLeg ;
				PfWeight += QPFullWeight[qpidx] * PfM[qpidx] ;

				for (int ipf = 0 ; ipf < NSlater; ipf ++) {
					PfDerMP.at(ipf) += PfDerQPfull.at(ipf + qpidx * NSlater) ;
				}
			}
			// FTTN
			double FTTNweight = fttn.compute_weight(eleNum_shift) ;
			vector<double> FTTNderMP(NPEPSelem, 0.0) ;
			fttn.derivative( &(FTTNderMP[0]), &(eleNum_shift[0]), FTTNweight ) ;

			for(int k = 0; k < NProj; k++)
				srOptO[k + 1] += (double)(projCnt.at(k)) * ProjWeight * PfWeight * FTTNweight ;

			for ( int k = 0;  k < NSlater; k ++) {
				int ko = k + NProj + 1 ;
				srOptO[ko] += PfDerMP.at(k) * ProjWeight * FTTNweight ;
			}

			for ( int k = 0; k < NPEPSelem; k ++ ) {
				int ko = k + NProj + NSlater + 1 ;
				srOptO[ko] += FTTNderMP.at(k) * FTTNweight * ProjWeight * PfWeight ;
			}
		}
		// normalize the derivative
		for ( int k = 0 ; k < NPara ; k ++) {
			srOptO[k + 1] /= weight ;
		}
		//=================================================================
		StartTimer(43);
		/* Calculate OO and HO */
		// Calculate <O>
		calculateO(SROptO, srOptO, SROptSize) ;
		// Calculate <HO>
		calculateHO(SROptHO, srOptO, w, local_energy, SROptSize) ;
		// Calculate diagonal elements of <OO>
		calculateOOdiag(SROptOOdiag, srOptO, SROptSize) ;
		// Calculate diagonal elements of <OOO>
		calculateOpowDiag(SROptOOOdiag, srOptO, 3, SROptSize) ;
		// Calculate diagonal elements of <OOOO>
		calculateOpowDiag(SROptOOOOdiag, srOptO, 4, SROptSize) ;
		StopTimer(43);
	} /* end of for(sample) */
	return;
}

void VMCMainCal_symCorFactor(MPI_Comm comm, FatTreeTensorNetwork& fttn)  {
	int *eleIdx,*eleCfg,*eleNum;
	double local_energy ;
	double w;
	double we,sqrtw;

	const int qpStart=0;
	const int qpEnd=NQPFull;
	int sample, sampleStart, sampleEnd, sampleSize;
	int info;

	/* optimazation for Kei */
	const int nProj=NProj;
	double *srOptO = SROptO;
	int *srOptO_ind ;

	int rank, size, int_i;
	MPI_Comm_size(comm,&size);
	MPI_Comm_rank(comm,&rank);

	SplitLoop(&sampleStart,&sampleEnd,NVMCSample,rank,size);

	/* initialization */
	clearPhysQuantity();
	for(sample=sampleStart;sample<sampleEnd;sample++) {
		eleIdx = EleIdx + sample*Nsize;
		eleCfg = EleCfg + sample*Nsite2;
		eleNum = EleNum + sample*Nsite2;
		//-------------------------------------
		StartTimer(40);
		info = CalculateMAll(eleIdx,qpStart,qpEnd);
		StopTimer(40);

		if(info!=0) {
			fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d info:%d (CalculateMAll)\n",rank,sample,info);
			continue;
		}

		double ip = CalculateIP(PfM,qpStart,qpEnd,MPI_COMM_SELF);
		double weight = CalculateWeight_symCorFactor(ip, eleNum, fttn, qpStart, qpEnd, MPI_COMM_SELF ) ;
//		fttn.initialize(eleNum) ;
		w = 1.0 ;

		StartTimer(41);
		/* calculate energy */
		local_energy = CalculateHamiltonian_symCorFactor(weight,eleIdx,eleCfg,eleNum, fttn);
		StopTimer(41);
		if( !isfinite(local_energy) ) {
			fprintf(stderr,"waring: VMCMainCal rank:%d sample:%d e=%e\n",rank,sample,local_energy);
			continue;
		}

		Wc += w;
		Etot  += w * local_energy;
		Etot2 += w * local_energy * local_energy;

		srOptO = SROptO_Store + sample * SROptSize ;
		srOptO[0] = 1.0;
		//---------------------------------------------------------
		vector<int> projCnt(NProj, 1.0) ;
	  vector<int> eleNum_shift(2*Nsite, -1) ;
		for ( int i = 0 ; i < NMPTrans; i ++ ) { // loop to create translational samples
			int * mqp = QPTrans[i] ;
			eleNum_shift = createSftEleNum(mqp, eleNum) ;
			// correlation factors
			MakeProjCnt(&(projCnt[0]), &(eleNum_shift[0])) ;
			double ProjWeight = ProjVal(&(projCnt[0])) ;

			// FTTN
			double FTTNweight = fttn.compute_weight(eleNum_shift) ;
			vector<double> FTTNderMP(NPEPSelem, 0.0) ;
			fttn.derivative( &(FTTNderMP[0]), &(eleNum_shift[0]), FTTNweight ) ;

			for(int k = 0; k < NProj; k++)
				srOptO[k + 1] += (double)(projCnt.at(k)) * ProjWeight * ip * FTTNweight ;

			for ( int k = 0; k < NPEPSelem; k ++ ) {
				int ko = k + NProj + NSlater + 1 ;
				srOptO[ko] += FTTNderMP.at(k) * FTTNweight * ProjWeight * ip ;
			}
		}
		// normalize the derivative
		for ( int k = 0 ; k < NPara ; k ++) {
			srOptO[k + 1] /= weight ;
		}

		SlaterElmDiff(srOptO+NProj+1, ip, eleIdx);
    //***********************debug***************************
//    for (int i = 0; i < NPara; i ++) {
//    	if ( srOptO[i + 1] != 0 )
//    		cout << i << ": " << srOptO[i + 1] << endl ;
//    }
//    exit(0) ;
    //***********************************************************
		//=================================================================
		StartTimer(43);
		/* Calculate OO and HO */
		// Calculate <O>
		calculateO(SROptO, srOptO, SROptSize) ;
		// Calculate <HO>
		calculateHO(SROptHO, srOptO, w, local_energy, SROptSize) ;
		// Calculate diagonal elements of <OO>
		calculateOOdiag(SROptOOdiag, srOptO, SROptSize) ;
		// Calculate diagonal elements of <OOO>
		calculateOpowDiag(SROptOOOdiag, srOptO, 3, SROptSize) ;
		// Calculate diagonal elements of <OOOO>
		calculateOpowDiag(SROptOOOOdiag, srOptO, 4, SROptSize) ;
		StopTimer(43);
	} /* end of for(sample) */
	return;
}

void output_energy_sample(std::vector<double>& energy_sample) {
	string fileName = "local_energy.dat" ;
	ofstream outfile(&(fileName[0]), ios::out) ;
	if (!outfile) {
		cerr << "output_energy_sample: open error!" << endl ;
		exit(0) ;
	}
	outfile.precision(16) ;
	for ( int i = 0; i < energy_sample.size(); i ++ ) {
		outfile << energy_sample[i] << endl ;
	}
}

void clearPhysQuantity(){
  int i,n;
  double *vec;
  int *ind ;
  Wc = Etot = Etot2 = 0.0;
  Etot1 = 0.0 ;
  if(NVMCCalMode==0) {
  	if (useSRCG) {
      /* SROptHO */
      n = SROptSize;
      vec = SROptHO;
      for(i=0;i<n;i++) vec[i] = 0.0;
      // SROptHO_SD ;
      vec = SROptHO_SD ;
      for(i=0;i<n;i++) vec[i] = 0.0;

      /* SROptO */
      vec = SROptO;
      for(i=0;i<n;i++) vec[i] = 0.0;

      // SROptOOdiag
      vec = SROptOOdiag ;
      for (i = 0; i < n; i ++) vec[i] = 0.0 ;
      // SROptOOOdiag
      vec = SROptOOOdiag ;
      for ( i = 0; i < n; i ++ ) vec[i] = 0.0 ;
      // SROptOOOOdiag
      vec = SROptOOOOdiag ;
      for ( i = 0; i < n; i ++ ) vec[i] = 0.0 ;

      // SROptO_Store
      if ( isSparseO ) {
      	n = SROptSize_sparse*NVMCSample ;
      	ind = SROptO_ind ;
      	for(i=0;i<n;i++) ind[i] = -1 ;
      }
      else n = SROptSize*NVMCSample ;

      vec = SROptO_Store;
      for(i=0;i<n;i++) vec[i] = 0.0;
  	}
  	else {
      /* SROptOO, SROptHO, SROptO */
      n = SROptSize*(SROptSize+2);
      vec = SROptOO;
      for(i=0;i<n;i++) vec[i] = 0.0;
  	}
  } else if(NVMCCalMode==1) {
    /* CisAjs, CisAjsCktAlt, CisAjsCktAltDC */
    n = 2*NCisAjs+NCisAjsCktAlt+NCisAjsCktAltDC;
    vec = PhysCisAjs;
    for(i=0;i<n;i++) vec[i] = 0.0;
    if(NLanczosMode>0) {
      /* QQQQ, LSLQ */
      n = NLSHam*NLSHam*NLSHam*NLSHam + NLSHam*NLSHam;
      vec = QQQQ;
      for(i=0;i<n;i++) vec[i] = 0.0;
      if(NLanczosMode>1) {
        /* QCisAjsQ, QCisAjsCktAltQ, LSLCisAjs */
        n = NLSHam*NLSHam*NCisAjs + NLSHam*NLSHam*NCisAjsCktAlt
          + NLSHam*NCisAjs;
        vec = QCisAjsQ;
        for(i=0;i<n;i++) vec[i] = 0.0;
      }
    }
  }
  return;
}

void calculateOptTransDiff(double *srOptO, const double ipAll) {
  int i,j;
  double ip;
  double *pfM;

  for(i=0;i<NQPOptTrans;++i) {
    ip = 0.0;
    pfM = PfM + i*NQPFix;
    for(j=0;j<NQPFix;++j) {
      ip += QPFixWeight[j] * pfM[j];
    }
    srOptO[i] = ip/ipAll;
  }

  return;
}

void calculateOO_Store(double *srOptOO, double *srOptHO, double *srOptO_Store,
                 const double w, const double e, int srOptSize, int sampleSize)
{
  //#define M_DGEM dgemm_
//  extern int dgemm_(char *jobz, char *uplo, int *m,int *n,int *k,double *alpha,  double *a, int *lda, double *b, int *ldb,
//                    double *beta,double *c,int *ldc);
  char jobz, uplo;
  double alpha,beta;

  alpha = 1.0;
  beta  = 0.0;

  jobz = 'N';
  uplo = 'T';
  dgemm_(&jobz,&uplo,&srOptSize,&srOptSize,&sampleSize,&alpha,srOptO_Store,&srOptSize,srOptO_Store,&srOptSize,&beta,srOptOO,&srOptSize);

  return;
}

void calculateOO(double *srOptOO, double *srOptHO, const double *srOptO,
                 const double w, const double e, const int srOptSize) {
  double we=w*e;

//  #define M_DAXPY daxpy_
	#define M_DAXPY daxpy
//  #define M_DGER dger_
	#define M_DGER dger

//  extern int M_DAXPY(const int *n, const double *alpha, const double *x, const int *incx,
//                     double *y, const int *incy);
//  extern int M_DGER(const int *m, const int *n, const double *alpha,
//                    const double *x, const int *incx, const double *y, const int *incy,
//                    double *a, const int *lda);
  int m,n,incx,incy,lda;
  m=n=lda=srOptSize;
  incx=incy=1;
  //=============================================
  double Oi, Oj ;
  for (int i = 0; i < srOptSize; i ++)
  {
  	Oi = srOptO[i] ;
  	if (Oi == 0)
  	{
  		continue ;
  	}
  	Oi *= w ;
  	for (int j = 0; j < srOptSize; j ++)
  	{
  		Oj = srOptO[j] ;
  		if (Oj == 0)
  		{
  			continue ;
  		}
  		srOptOO[j + i * srOptSize] += Oi * Oj ;
  	}
  	srOptHO[i] += e * Oi ;
  }
  //=============================================
  /* OO[i][j] += w*O[i]*O[j] */
//  M_DGER(&m, &n, &w, srOptO, &incx, srOptO, &incy, srOptOO, &lda);
//  dger(&m, &n, &w, srOptO, &incx, srOptO, &incy, srOptOO, &lda);

  /* HO[i] += w*e*O[i] */
//  M_DAXPY(&n, &we, srOptO, &incx, srOptHO, &incy);
//  daxpy(&n, &we, srOptO, &incx, srOptHO, &incy);

  return;
}

void calculateO(double *srOpt_total, const double *srOptO, const int srOptSize)
{
	int n = srOptSize ;
	double a = 1.0 ;
	int incx = 1 ;
	int incy = 1 ;
	// srOpt_total[i] += srOptO[i]
	daxpy_(&n, &a, srOptO, &incx, srOpt_total, &incy) ;
}
void calculateO(double *srOpt_total, const double *srOptO, const int *srOptO_ind, const int srOptSize_sparse)
{ // srOpt_total[ srOptO_ind[i] ] += srOptO[ i ]
	// srOptO is stored in sparse form
	for ( int i = 0 ; i < srOptSize_sparse; i ++ ) {
		srOpt_total[ srOptO_ind[i] ] += srOptO[ i ] ;
	}
}
//---------------------------------------------------------------------
void calculateHO(double *srOptHO, const double *srOptO, const double w,
								 const double local_energy, const int srOptSize)
{
	double we = w * local_energy ;
	int n = srOptSize ;
	int incx = 1 ;
	int incy = 1 ;
	// HO[i] += w*e*O[i]
	daxpy_(&n, &we, srOptO, &incx, srOptHO, &incy);
}
void calculateHO(double *srOptHO, const double *srOptO, const int *srOptO_ind, const double w,
								 const double local_energy, const int srOptSize_sparse)
{
	double we = w * local_energy ;
	for ( int i = 0 ; i < srOptSize_sparse; i ++ ) {
		srOptHO[ srOptO_ind[i] ] += we * srOptO[ i ] ;
	}
}
//---------------------------------------------------------------------
void calculateOOdiag(double *srOptOOdiag, const double *srOptO, const int srOptSize) {
	for (int i = 0; i < srOptSize; i ++) {
		srOptOOdiag[i] += srOptO[i] * srOptO[i] ;
	}
}

void calculateOpowDiag(double *srOptOpowDiag, const double *srOptO, const double p, const int srOptSize) {
	for ( int i = 0 ; i < srOptSize; i ++ ) {
		srOptOpowDiag[i] += pow(srOptO[i], p) ; // O(i)^p
	}
}

void calculateOOdiag(double *srOptOOdiag, const double *srOptO, const int *srOptO_ind, const int srOptSize_sparse)
{
	for (int i = 0; i < srOptSize_sparse; i ++) {
		srOptOOdiag[srOptO_ind[i]] += srOptO[i] * srOptO[ i ] ;
	}
}

//---------------------------------------------------------------------

void calculateQQQQ(double *qqqq, const double *lslq, const double w, const int nLSHam) {
  const int n=nLSHam*nLSHam*nLSHam*nLSHam;
  int rq,rp,ri,rj;
  int i,tmp;

  /* QQQQ[rq][rp][ri][rj] += w * LSLQ[rq][ri] * LSLQ[rp][rj] */
  # pragma omp parallel for default(shared) private(i,tmp,rq,rp,ri,rj)
  for(i=0;i<n;++i) {
    rj = i%nLSHam;   tmp=i/nLSHam;
    ri = tmp%nLSHam; tmp=tmp/nLSHam;
    rp = tmp%nLSHam; tmp=tmp/nLSHam;
    rq = tmp%nLSHam;

    qqqq[i] += w * lslq[rq*nLSHam+ri] * lslq[rp*nLSHam+rj];
  }

  return;
}

void calculateQCAQ(double *qcaq, const double *lslca, const double *lslq,
                   const double w, const int nLSHam, const int nCA) {
  const int n=nLSHam*nLSHam*nCA;
  int rq,rp,idx;
  int i,tmp;

  /* QCisAjsQ[rq][rp][idx] += w * LSLCisAjs[rq][idx] * LSLQ[rp][0] */
# pragma omp parallel for default(shared) private(i,tmp,idx,rp,rq)
  for(i=0;i<n;++i) {
    idx = i%nCA;     tmp = i/nCA;
    rp = tmp%nLSHam; tmp = tmp/nLSHam;
    rq = tmp%nLSHam;

    qcaq[i] += w * lslca[rq*nCA+idx] * lslq[rp*nLSHam];
  }

  return;
}

void calculateQCACAQ(double *qcacaq, const double *lslca, const double w,
                     const int nLSHam, const int nCA, const int nCACA,
                     int **cacaIdx) {
  const int n=nLSHam*nLSHam*nCACA;
  int rq,rp,ri,rj,idx;
  int i,tmp;

  /* QCisAjsCktAltQ[rq][rp][idx] += w * LSLCisAjs[rq][ri] * LSLCisAjs[rp][rj] */
# pragma omp parallel for default(shared) private(i,tmp,idx,rp,rq,ri,rj)
  for(i=0;i<n;++i) {
    idx = i%nCACA;   tmp = i/nCACA;
    rp = tmp%nLSHam; tmp = tmp/nLSHam;
    rq = tmp%nLSHam;

    ri = cacaIdx[idx][0];
    rj = cacaIdx[idx][1];

    qcacaq[i] += w * lslca[rq*nCA+ri] * lslca[rp*nCA+rj];
  }

  return;
}

double findStepWid_LS(MPI_Comm comm) {
	int rank, size;
	MPI_Comm_size(comm,&size);
	MPI_Comm_rank(comm,&rank);

//	if(rank==0) {
//		cout << "H * I = " << QQQQ[2]  ;
//		cout << ", H * H = " << QQQQ[3] ;
//		cout << ", H^2 * I = " << QQQQ[10] ;
//		cout << ", H^2 * H = " << QQQQ[11] << endl ;
//	}
//  double H      = QQQQ[2] ;
//  double H2_1   = QQQQ[3] ;
//  double H2_2   = QQQQ[10] ;
//  double H3     = QQQQ[11];
	double a   = QQQQ[2] ;
	double b   = QQQQ[3] ;
	double c   = QQQQ[10] ;
	double d   = QQQQ[11];
	double tmp_AA  = b*(b+c) - 2*a*d;
	double tmp_BB  = -a*b + d;
	double tmp_CC  = b*(b+c)*(b+c) - a*a*b*(b+2*c) + 4*(a*a*a)*d - 2*a*(2*b+c)*d + d*d;

  double alpha_p  = (tmp_BB+sqrt(tmp_CC))/tmp_AA;
  double alpha_m  = (tmp_BB-sqrt(tmp_CC))/tmp_AA;


  double tmp_1 = a + alpha_p*(b+c) + alpha_p*alpha_p*d;
  double tmp_2 = 1.0 + 2*alpha_p*a + alpha_p*alpha_p*b;
  double energy_p = tmp_1 / tmp_2 ;

  tmp_1 = a + alpha_m*(b+c) + alpha_m*alpha_m*d;
  tmp_2 = 1.0 + 2*alpha_m*a + alpha_m*alpha_m*b;
  double energy_m = tmp_1 / tmp_2 ;

  double stepWid ;
  if(rank==0) {
  	if ( energy_p < energy_m ) {
  		cout << "1st Lanczos E = " << energy_p ;
//  		cout << ", alpha = " << alpha_p << endl ;
  		stepWid = - alpha_p / 2 ;
  	} else {
  		cout << "1st Lanczos E = " << energy_m ;
//  		cout << ", alpha = " << alpha_m << endl ;
  		stepWid = - alpha_m / 2 ;
  	}
  	cout << ", step width: " << stepWid << endl ;
  }

  if ( stepWid < 0) stepWid = minSteWid ;
  else if ( stepWid > maxSteWid ) stepWid = maxSteWid ;
//	return fabs(stepWid);
	return stepWid;
}

void find1stepLSenergy(MPI_Comm comm) {

	int rank, size;
	MPI_Comm_size(comm,&size);
	MPI_Comm_rank(comm,&rank);

	double a   = QQQQ[2] ;   // H * I
	double b   = QQQQ[3] ;   // H * H
	double c   = QQQQ[10] ;  // H^2 * I
	double d   = QQQQ[11];   // H^2 * H
	double tmp_AA  = b*(b+c) - 2*a*d;
	double tmp_BB  = -a*b + d;
	double tmp_CC  = b*(b+c)*(b+c) - a*a*b*(b+2*c) + 4*(a*a*a)*d - 2*a*(2*b+c)*d + d*d;

  double alpha_p  = (tmp_BB+sqrt(tmp_CC))/tmp_AA;
  double alpha_m  = (tmp_BB-sqrt(tmp_CC))/tmp_AA;


  double tmp_1 = a + alpha_p*(b+c) + alpha_p*alpha_p*d;
  double tmp_2 = 1.0 + 2*alpha_p*a + alpha_p*alpha_p*b;
  double energy_p = tmp_1 / tmp_2 ;

  tmp_1 = a + alpha_m*(b+c) + alpha_m*alpha_m*d;
  tmp_2 = 1.0 + 2*alpha_m*a + alpha_m*alpha_m*b;
  double energy_m = tmp_1 / tmp_2 ;

  if(rank==0) {
  	if ( energy_p < energy_m ) {
  		cout << "One step Lanczos E = " << energy_p ;
  	} else {
  		cout << "One step Lanczos E = " << energy_m ;
  	}
  	cout << endl ;
  }
}
