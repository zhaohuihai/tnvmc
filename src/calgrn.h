/*
 * calgrn.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_CALGRN_H_
#define SRC_CALGRN_H_

#include "correlator_product_state.h"
#include "fat_tree_tensor_network.h"

void CalculateGreenFunc(const double w, const double ip, int *eleIdx, int *eleCfg,
                         int *eleNum, int *eleProjCnt);
void CalculateGreenFunc(const double w, const double ip, int *eleIdx, int *eleCfg,
                        int *eleNum, int *eleProjCnt,
												CorrelatorProductState& cps);

void CalculateGreenFunc(const double w, const double ip, int *eleIdx, int *eleCfg,
                        int *eleNum, int *eleProjCnt,
												FatTreeTensorNetwork& fttn);

#endif /* SRC_CALGRN_H_ */
