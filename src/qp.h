/*
 * qp.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_QP_H_
#define SRC_QP_H_

#include "fat_tree_tensor_network.h"

void InitQPWeight();

double CalculateLogIP(double * const pfM, const int qpStart,
											const int qpEnd, MPI_Comm comm);
double CalculateLogWeight(double * const pfM, const int *eleNum, FatTreeTensorNetwork& fttn,
		const int qpStart, const int qpEnd, MPI_Comm comm) ;
double CalculateLogWeight_symCorFactor(const double logIp, const int *eleNum, FatTreeTensorNetwork& fttn,
		const int qpStart, const int qpEnd, MPI_Comm comm) ;

double CalculateIP(double * const pfM, const int qpStart,
									 const int qpEnd, MPI_Comm comm);
double CalculateWeight(double * const pfM, const int *eleNum, FatTreeTensorNetwork& fttn,
		const int qpStart, const int qpEnd, MPI_Comm comm) ;
double CalculateWeight_symCorFactor(const double pfa, const int *eleNum, FatTreeTensorNetwork& fttn,
		const int qpStart, const int qpEnd, MPI_Comm comm) ;

std::vector<int> createSftEleNum(const int* mqp, const int* eleNum) ;

void UpdateQPWeight();

#endif /* SRC_QP_H_ */
