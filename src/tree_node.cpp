/*
 * tree_node.cpp
 *
 *  Created on: 2016年2月29日
 *      Author: zhaohuihai
 */

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "vmctime.h"
#include "tree_node.h"

using namespace std ;

// constructor for leaf node
TreeNode::TreeNode(const int nsites, const std::vector<int>& sites, const int siteDim, const int bondDim,
					         double* elem, int offset) {
	_ti = false ;
	_siteDim = siteDim ; // dim of physical index
	_bondDim = bondDim ;
	_nsites = nsites ;
	_isLeaf = true ; // this is leaf node
	_sites = sites ;
	_nelements = _bondDim ;
	for ( int i = 0 ; i < sites.size(); i ++ ) {
		_nelements *= _siteDim ;
	}
	_offset = offset ; // position of first element
	_elem = elem ; // the pointer to the first element of this node
	_dims.resize(1 + sites.size()) ;
	_dims.at(0) = _bondDim ;
	for ( int i = 1; i < _dims.size(); i ++ ) _dims.at(i) = _siteDim ;

	_parent = -10 ;
	_children.clear() ;
}

TreeNode::TreeNode(const int nsites, const std::vector<int>& sites, const int siteDim, const int bondDim,
		               int parent, double* elem, int offset) {
	_ti = false ;
	_siteDim = siteDim ; // dim of physical index
	_bondDim = bondDim ;
	_nsites = nsites ;
	_isLeaf = true ; // this is leaf node
	_sites = sites ;
	_nelements = _bondDim ;
	for ( int i = 0 ; i < sites.size(); i ++ ) {
		_nelements *= _siteDim ;
	}
	_offset = offset ; // position of first element
	_elem = elem ; // the pointer to the first element of this node
	_dims.resize(1 + sites.size()) ;
	_dims.at(0) = _bondDim ;
	for ( int i = 1; i < _dims.size(); i ++ ) _dims.at(i) = _siteDim ;

	_parent = parent ;
	_children.clear() ;
}

TreeNode::TreeNode(const int bondDim, double* elem, int offset, const int rank) {
	_ti = false ;
	_siteDim = 0 ; // no physical index
	_bondDim = bondDim ;
	_isLeaf = false ; // this is not leaf node
	_nsites = 0 ; // no physical sites

	_dims.resize(rank) ;
	_nelements = 1 ;
	for ( int i = 0; i < rank; i ++ ) {
		_nelements *= bondDim ;
		_dims[i] = bondDim ;
	}

	_offset = offset ; // position of first element
	_elem = elem ;

	_parent = -10 ;
	_children.assign(rank - 1, -10) ;
}

TreeNode::TreeNode(const int bondDim, int parent, std::vector<int> children, double* elem, int offset) {
	_ti = false ;
	_siteDim = 0 ; // no physical index
	_bondDim = bondDim ;
	_isLeaf = false ; // this is not leaf node
	_nsites = 0 ; // no physical sites

	int rank ;
	if ( parent == -1 ) rank = children.size() ;
	else rank = children.size() + 1 ;
	_dims.resize(rank) ;
	_nelements = 1 ;
	for ( int i = 0; i < rank; i ++ ) {
		_nelements *= bondDim ;
		_dims[i] = bondDim ;
	}
	_offset = offset ; // position of first element
	_elem = elem ;

	_parent = parent ;
	_children = children ;
}

// get leaf tensor with fixed physical indices
vector<double> TreeNode::get_leaf(const int* eleNum) {
	if ( !_isLeaf ) {
		cout << "get_leaf error: not a leaf node" << endl ;
		exit(0) ;
	}
	// T(a0,i0,i1,i4,i5), T(a0,i0,i1)
	int n = 0 ; // position of first element
	int base = _dims.at(0) ;
	for ( int i = 0 ; i < _sites.size(); i ++ ) {
		int iLattice = _sites.at(i) ;
		n += getPhysInd(eleNum, iLattice) * base ;
		base *= _dims.at( i + 1 ) ;
	}
	vector<double> leaf(_elem + n, _elem + n + _bondDim) ;
//	Tensor<double> leaf(_bondDim) ;
//	leaf.setElem( &(_elem[n]) ) ;
	return leaf ;
}

vector<double> TreeNode::get_leaf(const std::vector<int>& eleNum) {
	int n = 0 ; // position of first element
	int base = _dims.at(0) ;
	for ( int i = 0 ; i < _sites.size(); i ++ ) {
			int iLattice = _sites.at(i) ;
			n += getPhysInd(&(eleNum[0]), iLattice) * base ;
			base *= _dims.at( i + 1 ) ;
	}
	vector<double> leaf(_elem + n, _elem + n + _bondDim) ;
	return leaf ;
}

//void TreeNode::get_leaf(const vector<int>& eleNum, vector<double>& leaf) {
//	int n = 0 ; // position of first element
//	int base = _dims.at(0) ;
//	for ( int i = 0 ; i < _sites.size(); i ++ ) {
//			int iLattice = _sites.at(i) ;
//			n += getPhysInd(&(eleNum[0]), iLattice) * base ;
//			base *= _dims.at( i + 1 ) ;
//	}
//	if ( leaf.size() != _bondDim ) leaf.resize(_bondDim) ;
//	leaf.assign((_elem + n), (_elem + n + _bondDim)) ;
//}

bool TreeNode::isIncluded(const std::vector<int>& sites){
	if ( !_isLeaf ) {
		cout << "get_leaf error: not a leaf node" << endl ;
		exit(0) ;
	}

	for ( int i = 0; i < _sites.size(); i ++) {
		for ( int j = 0; j < sites.size(); j ++ ) {
			if ( _sites.at(i) == sites.at(j) ) return true ;
		}
	}
	return false ;
}

void TreeNode::assign(int dimFile, std::vector<double>& nodeFile) {
	if ( _isLeaf ) { // leaf node
		int a, af, b ;
		for ( int i = 0; i < _nelements; i ++ ) { // i ~ (a,(b1,b2,b3,b4))
			a = i % _bondDim ;
			b = i / _bondDim ; // b ~ (b1,b2,b3,b4)
			af = a % dimFile ;
			_elem[i] = nodeFile[af + b * dimFile] ;
		}
	} else if ( _dims.size() == 3 ) { // internal node
		int a0,a1,a2, af0,af1,af2 ;
		int r, j ;
		for ( int i = 0; i < _nelements; i ++ ) { // i ~ (a0,a1,a2)
			a0 = i % _bondDim ;
			r = (i - a0) / _bondDim ; // r = a1 + a2*D
			a1 = r % _bondDim ;
			a2 = r / _bondDim ;

			af0 = a0 % dimFile ;
			af1 = a1 % dimFile ;
			af2 = a2 % dimFile ;
			j = af0 + af1 * dimFile + af2 * dimFile * dimFile ;
			_elem[i] = nodeFile[j] ;
		}
	} else if ( _dims.size() == 2 ) { // root node
		int a0,a1,af0,af1 ;
		int j ;
		for ( int i = 0; i < _nelements; i ++ ) { // i ~ (a0,a1)
			a0 = i % _bondDim ;
			a1 = i / _bondDim ;

			af0 = a0 % dimFile ;
			af1 = a1 % dimFile ;
			j = af0 + af1 * dimFile ;
			_elem[i] = nodeFile[j] ;
		}
	}
}

vector<double> TreeNode::get_tensor() {
	vector<double> T(_elem, _elem + _nelements) ;
	return T ;
}

vector<double> TreeNode::contractLeaves(vector<double>& lf0, vector<double>& lf1) {
	assert(!_isLeaf) ; // not a leaf node
//	assert(_dims.size() == 3) ; // not root
	int m = _dims[0] * _dims[1] ; // (a16,a0)
	int n = _dims[2] ; //
	vector<double> buff(m) ;
	vector<double> T_new(_bondDim) ;
	xgemv('N', m, n, _elem, &(lf1[0]), &(buff[0])) ; // buff(a16,a0) = sum{a1}_[_elem(a16,a0,a1)*lf1(a1)]
	xgemv('N', _dims[0], _dims[1], &(buff[0]), &(lf0[0]), &(T_new[0])) ; // T_new(a16) = sum{a0}_[buff(a16,a0)*lf0(a0)]

	return T_new ;
}

std::vector<double> TreeNode::contractLeaves(std::vector<double>& lf0, std::vector<double>& lf1,
																   std::vector<double>& buff) {
	assert(!_isLeaf) ; // not a leaf node
//	assert(_dims.size() == 3) ; // not root
	int m = _dims[0] * _dims[1] ; // (a16,a0)
	int n = _dims[2] ; //
	vector<double> branch(_bondDim) ;
	xgemv('N', m, n, _elem, &(lf1[0]), &(buff[0])) ; // buff(a16,a0) = sum{a1}_[_elem(a16,a0,a1)*lf1(a1)]
	xgemv('N', _dims[0], _dims[1], &(buff[0]), &(lf0[0]), &(branch[0])) ; // T_new(a16) = sum{a0}_[buff(a16,a0)*lf0(a0)]

	return branch ;
}

void TreeNode::contractLeaves(std::vector<double>& lf0,    std::vector<double>& lf1,
														  std::vector<double>& branch, std::vector<double>& buff) {
	assert(!_isLeaf) ; // not a leaf node
	int m = _dims[0] * _dims[1] ; // (a16,a0)
	int n = _dims[2] ; //
	xgemv('N', m, n, _elem, &(lf1[0]), &(buff[0])) ; // buff(a16,a0) = sum{a1}_[_elem(a16,a0,a1)*lf1(a1)]
	xgemv('N', _dims[0], _dims[1], &(buff[0]), &(lf0[0]), &(branch[0])) ; // T_new(a16) = sum{a0}_[buff(a16,a0)*lf0(a0)]
}

// weight = sum{l,r}_[_elem(l,r) * leftBranch(l) * rightBranch(r)]
double TreeNode::contractRoot(vector<double>& leftBranch, vector<double>& rightBranch) {
	assert(_dims.size() == 2) ; // root

	int m = _bondDim ;
	int n = _bondDim ;
	vector<double> buff(m) ;
	xgemv('N', m, n, _elem, &(rightBranch[0]), &(buff[0]) ) ; // buff(l) = sum{r} _[_elem(l,r) * rightBranch(r)]
	double weight = xdot(m, &(buff[0]), &(leftBranch[0])) ;// weight = sum{l}_[buff(l) * leftBranch(l)]
	//------------------------------------------------------
	if ( fabs(weight) < 1e-300 ) weight = 1e-300 ;
	//------------------------------------------------------
	return weight ;
}

int TreeNode::getPhysInd(const int* eleNum, const int i) {
	int physInd ;
	int upInd, downInd ;
	upInd = eleNum[i] ;
	downInd = eleNum[i + _nsites] ;
	physInd = upInd + downInd * 2 ;
//	std::cout << "physInd: " << physInd << std::endl ;
	if ( physInd < 0 || physInd >= _siteDim) {
		std::cout << "TreeNode::getPhysInd error: " ;
		std::cout << "physInd = " << physInd << ", which is out of range." << std::endl ;
		exit(0) ;
	}
	return physInd ;
}

void TreeNode::rescale(double maxElem) {
	if ( _nelements == 0 ) return ;
//	double max_value ;
//	int incx = 1 ;
//	int index = idamax_(&_nelements, _elem, &incx) ;
//	max_value = _elem[index-1] ;
//	//-------------------------------------------
////	if ( _isLeaf ) {
////		maxElem /= (double)_bondDim ;
////	} else if ( _dims.size() == 5 ) {
////		maxElem /= pow((double)_bondDim, 5.0) ;
////	} else {
////		maxElem /= pow((double)_bondDim, 4.0) ;
////	}
//	//-------------------------------------------
//	xscal(_nelements, maxElem/max_value, _elem) ;
//	//-------------------------------------------
	double a = 0 ;
	for ( int i = 0; i < _nelements; i ++ ) {
		a += _elem[i] * _elem[i] ;
	}
	double normFactor ;
	if ( _isLeaf )  {
		normFactor = pow(_siteDim, _sites.size()) ;
	} else {
		normFactor = 1 ;
//		normFactor = pow(_bondDim, _dims.size() - 1 ) ;
	}
	normFactor = sqrt(normFactor / a) ;
	xscal(_nelements, maxElem * normFactor, _elem) ;
}

void TreeNode::info() {
	if ( _isLeaf ) {
		for (int i = 0; i < _sites.size(); i ++)
			cout << _sites.at(i) << ", " ;
		cout << endl ;
	}
	cout << "parent node: " << _parent << endl ;
	cout << "children nodes: " ;
	for ( int i = 0; i < _children.size(); i ++ ) {
		cout << _children.at(i) << ", " ;
	}
	cout << endl ;
}

void TreeNode::disp() {
	for (int i = 0; i < _nelements; i ++) {
		cout << _elem[i] << ", " ;
	}
	cout << endl << endl ;
}
