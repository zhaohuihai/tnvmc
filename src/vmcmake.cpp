/*-------------------------------------------------------------
 * Variational Monte Carlo
 * make sample
 *-------------------------------------------------------------
 * by Satoshi Morita
 *-------------------------------------------------------------*/

#include <vector>
#include "vmcmain.h"

#include "splitloop.h"
#include "vmctime.h"
#include "matrix.h"
#include "qp.h"
#include "projection.h"
#include "pfupdate.h"
#include "pfupdate_two.h"
#include "sfmt/SFMT.h"

#include "vmcmake.h"

using namespace std ;

void VMCMakeSample(MPI_Comm comm)
{
  int outStep,nOutStep;
  int inStep,nInStep;
  int updateType;
  int mi,mj,ri,rj,s,t,i;
  int nAccept=0;
  int sample;

  double logIpOld,logIpNew; /* logarithm of inner product <phi|L|x> */
//  int projCntNew[NProj];
  vector<int> projCntNew(NProj, 0) ;
//  double pfMNew[NQPFull];
  vector<double> pfMNew(NQPFull, 0) ;
  double x,w;

  int qpStart,qpEnd;
  int rank,size;
  MPI_Comm_size(comm,&size);
  MPI_Comm_rank(comm,&rank);

  SplitLoop(&qpStart,&qpEnd,NQPFull,rank,size);

  StartTimer(30);
  if(BurnFlag==0) {
    makeInitialSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt,
                      qpStart,qpEnd,comm);
  } else {
    copyFromBurnSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
  }
  CalculateMAll(TmpEleIdx,qpStart,qpEnd);

  logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);
  if( !isfinite(logIpOld) ) {
    if(rank==0) fprintf(stderr,"waring: VMCMakeSample remakeSample logIpOld=%e\n",logIpOld);
    makeInitialSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt,
                      qpStart,qpEnd,comm);
    CalculateMAll(TmpEleIdx,qpStart,qpEnd);
    logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);
    BurnFlag = 0;
  }
  StopTimer(30);

  nOutStep = (BurnFlag==0) ? NVMCWarmUp+NVMCSample : NVMCSample+1;
  nInStep = NVMCIniterval * Nsite;

  for(i=0;i<4;i++) Counter[i]=0;  /* reset counter */

  for(outStep=0;outStep<nOutStep;outStep++) {
    for(inStep=0;inStep<nInStep;inStep++) {

      StartTimer(31);
      /* generate the candidate configuration */
      updateType = -1;
      while(updateType<0) {
        updateType=0; /* hopping */
        if(NExUpdatePath==1) {
          if(genrand_real2()<0.5) updateType = 1; /* exchange */
        }
        mi = gen_rand32()%Ne;
        s = (genrand_real2()<0.5) ? 0 : 1;
        ri = TmpEleIdx[mi+s*Ne];
        do {
          rj = gen_rand32()%Nsite;
        } while (TmpEleCfg[rj+s*Nsite] != -1);

        /* check */
        if(updateType==0) {
          /* The mi-th electron with spin s hops to site rj */
          if(LocSpn[ri]==0 || LocSpn[rj]==0) updateType=-1;
        } else {
          /* The mi-th electron with spin s exchanges with the electron on site rj with spin 1-s */
          if(TmpEleCfg[rj+(1-s)*Nsite] == -1 || TmpEleCfg[ri+(1-s)*Nsite] != -1) updateType=-1;
        }
      }
      StopTimer(31);
      
      if(updateType==0) { /* hopping */
        StartTimer(32);
        /* The mi-th electron with spin s hops to site rj */
        TmpEleIdx[mi+s*Ne] = rj; // Ne is num of up spin electron
        TmpEleCfg[ri+s*Nsite] = -1;
        TmpEleCfg[rj+s*Nsite] = mi;
        TmpEleNum[ri+s*Nsite] = 0;
        TmpEleNum[rj+s*Nsite] = 1;

          StartTimer(60);
        UpdateProjCnt(ri,rj,s,&(projCntNew[0]),TmpEleProjCnt,TmpEleNum);
          StopTimer(60);
          StartTimer(61);
        CalculateNewPfM2(mi,s,&(pfMNew[0]),TmpEleIdx,qpStart,qpEnd);
          StopTimer(61);

          StartTimer(62);
        /* calculate inner product <phi|L|x> */
        logIpNew = CalculateLogIP(&(pfMNew[0]),qpStart,qpEnd,comm);
          StopTimer(62);

        /* Metroplis */
        x = LogProjRatio(&(projCntNew[0]),TmpEleProjCnt);
        w = exp(2.0*(x+logIpNew-logIpOld));
        if( !isfinite(w) ) w = -1.0; /* should be rejected */

        if(w > genrand_real2()) { /* accept */
            StartTimer(63);
            UpdateMAll(mi,s,TmpEleIdx,qpStart,qpEnd);
            StopTimer(63);

          for(i=0;i<NProj;i++) TmpEleProjCnt[i] = projCntNew[i];
          logIpOld = logIpNew;
          nAccept++;
          Counter[1]++;
        } else { /* reject */
          TmpEleIdx[mi+s*Ne] = ri;
          TmpEleCfg[ri+s*Nsite] = mi;
          TmpEleCfg[rj+s*Nsite] = -1;
          TmpEleNum[ri+s*Nsite] = 1;
          TmpEleNum[rj+s*Nsite] = 0;
        }
        Counter[0]++;
        StopTimer(32);
      } else { /* exchange */
        StartTimer(33);
        StartTimer(65);
        /* The mi-th electron with spin s exchanges with the electron on site rj with spin 1-s */
        t = 1-s;
        mj = TmpEleCfg[rj+t*Nsite];
        /* The mi-th electron with spin s hops to rj */
        TmpEleIdx[mi+s*Ne] = rj;
        TmpEleCfg[ri+s*Nsite] = -1;
        TmpEleCfg[rj+s*Nsite] = mi;
        TmpEleNum[ri+s*Nsite] = 0;
        TmpEleNum[rj+s*Nsite] = 1;
        UpdateProjCnt(ri,rj,s,&(projCntNew[0]),TmpEleProjCnt,TmpEleNum);
        /* The mj-th electron with spin t hops to ri */
        TmpEleIdx[mj+t*Ne] = ri;
        TmpEleCfg[rj+t*Nsite] = -1;
        TmpEleCfg[ri+t*Nsite] = mj;
        TmpEleNum[rj+t*Nsite] = 0;
        TmpEleNum[ri+t*Nsite] = 1;
        UpdateProjCnt(rj,ri,t,&(projCntNew[0]),&(projCntNew[0]),TmpEleNum);

        StopTimer(65);
        StartTimer(66);

        CalculateNewPfMTwo2(mi, s, mj, t, &(pfMNew[0]), TmpEleIdx, qpStart, qpEnd);

        StopTimer(66);
        StartTimer(67);

        /* calculate inner product <phi|L|x> */
        logIpNew = CalculateLogIP(&(pfMNew[0]),qpStart,qpEnd,comm);
        
        StopTimer(67);

        /* Metroplis */
        x = LogProjRatio(&(projCntNew[0]),TmpEleProjCnt);
        w = exp(2.0*(x+logIpNew-logIpOld));
        if( !isfinite(w) ) w = -1.0; /* should be rejected */
        
        if(w > genrand_real2()) { /* accept */
          StartTimer(68);
          UpdateMAllTwo(mi, s, mj, t, ri, rj, TmpEleIdx,qpStart,qpEnd);
          StopTimer(68);

/*           printf("%d %lf %lf\n",inStep,InvM[(mi+s*Ne)*Nsize+mj+t*Ne],InvM[(mi+s*Ne)*Nsize+mj+t*Ne+1]); */
/*           CalculateMAll(TmpEleIdx,qpStart,qpEnd); */
/*           printf("%d %lf %lf\n",inStep,InvM[(mi+s*Ne)*Nsize+mj+t*Ne],InvM[(mi+s*Ne)*Nsize+mj+t*Ne+1]); */

          for(i=0;i<NProj;i++) TmpEleProjCnt[i] = projCntNew[i];
          logIpOld = logIpNew;
          nAccept++;
          Counter[3]++;
        } else { /* reject */
          TmpEleIdx[mj+t*Ne] = rj;
          TmpEleCfg[rj+t*Nsite] = mj;
          TmpEleCfg[ri+t*Nsite] = -1;
          TmpEleNum[rj+t*Nsite] = 1;
          TmpEleNum[ri+t*Nsite] = 0;

          TmpEleIdx[mi+s*Ne] = ri;
          TmpEleCfg[ri+s*Nsite] = mi;
          TmpEleCfg[rj+s*Nsite] = -1;
          TmpEleNum[ri+s*Nsite] = 1;
          TmpEleNum[rj+s*Nsite] = 0;
        }
        Counter[2]++;
        StopTimer(33);
      }

      if(nAccept>Nsite) {
        StartTimer(34);
        /* recal PfM and InvM */
        CalculateMAll(TmpEleIdx,qpStart,qpEnd);
        logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);
        StopTimer(34);
        nAccept=0;
      }
    } /* end of instep */

    StartTimer(35);
    /* save Electron Configuration */
    if(outStep >= nOutStep-NVMCSample) {
      sample = outStep-(nOutStep-NVMCSample);
      saveEleConfig(sample,logIpOld,TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
    }
    StopTimer(35);

  } /* end of outstep */

  copyToBurnSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
  BurnFlag=1;
  return;
}

void VMCMakeSample(MPI_Comm comm, CorrelatorProductState& cps)
{
  int outStep,nOutStep;
  int inStep,nInStep;
  int updateType;
  int mi,mj,ri,rj,s,t,i;
  int nAccept=0;
  int sample;

  double logIpOld,logIpNew; /* logarithm of inner product <phi|L|x> */
  //  int projCntNew[NProj];
    vector<int> projCntNew(NProj, 0) ;
  //  double pfMNew[NQPFull];
    vector<double> pfMNew(NQPFull, 0) ;
  double x,w;
  double logCPSratio ;

  int qpStart,qpEnd;
  int rank,size;
  MPI_Comm_size(comm,&size);
  MPI_Comm_rank(comm,&rank);

  SplitLoop(&qpStart,&qpEnd,NQPFull,rank,size);

  StartTimer(30);
  if(BurnFlag==0) {
    makeInitialSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt,
                      qpStart,qpEnd,comm);
  } else {
    copyFromBurnSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
  }
  CalculateMAll(TmpEleIdx,qpStart,qpEnd);

//  cout << "eleNum: " ;
//  for (int j =0; j < Nsite2; j ++)
//  {
//  	cout << TmpEleNum[j] << ", " ;
//  }
//  cout << endl ;

  logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);

  if( !isfinite(logIpOld) ) {
    if(rank==0) fprintf(stderr,"waring: VMCMakeSample remakeSample logIpOld=%e\n",logIpOld);
    makeInitialSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt,
                      qpStart,qpEnd,comm);
    CalculateMAll(TmpEleIdx,qpStart,qpEnd);
    logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);
    BurnFlag = 0;
  }
  cps.initialize(TmpEleNum) ;
  StopTimer(30);

  nOutStep = (BurnFlag==0) ? NVMCWarmUp+NVMCSample : NVMCSample+1;
  nInStep = NVMCIniterval * Nsite;

  for(i=0;i<4;i++) Counter[i]=0;  /* reset counter */

  for(outStep=0;outStep<nOutStep;outStep++) {
    for(inStep=0;inStep<nInStep;inStep++) {

      StartTimer(31);
      /* generate the candidate configuration */
      updateType = -1;
      while(updateType<0) {
        updateType=0; /* hopping */
        if(NExUpdatePath==1) {
          if(genrand_real2()<0.5) updateType = 1; /* exchange */
        }
        mi = gen_rand32()%Ne;
        s = (genrand_real2()<0.5) ? 0 : 1; // spin
        ri = TmpEleIdx[mi+s*Ne];
        do {
          rj = gen_rand32()%Nsite;
        } while (TmpEleCfg[rj+s*Nsite] != -1);

        /* check */
        if(updateType==0) {
          /* The mi-th electron with spin s hops to site rj */
          if(LocSpn[ri]==0 || LocSpn[rj]==0) updateType=-1;
        } else {
          /* The mi-th electron with spin s exchanges with the electron on site rj with spin 1-s */
          if(TmpEleCfg[rj+(1-s)*Nsite] == -1 || TmpEleCfg[ri+(1-s)*Nsite] != -1) updateType=-1;
        }
      }
      StopTimer(31);

      if(updateType==0) { /* hopping */
        StartTimer(32);
        /* The mi-th electron with spin s hops to site rj */
        TmpEleIdx[mi+s*Ne] = rj; // Ne is num of up spin electron
        TmpEleCfg[ri+s*Nsite] = -1;
        TmpEleCfg[rj+s*Nsite] = mi;
        TmpEleNum[ri+s*Nsite] = 0;
        TmpEleNum[rj+s*Nsite] = 1;

          StartTimer(60);
        UpdateProjCnt(ri,rj,s,&(projCntNew[0]),TmpEleProjCnt,TmpEleNum);
          StopTimer(60);
          StartTimer(61);
        CalculateNewPfM2(mi,s,&(pfMNew[0]),TmpEleIdx,qpStart,qpEnd);
          StopTimer(61);

          StartTimer(62);
        /* calculate inner product <phi|L|x> */
        logIpNew = CalculateLogIP(&(pfMNew[0]),qpStart,qpEnd,comm);
          StopTimer(62);

        /* Metroplis */
        x = LogProjRatio(&(projCntNew[0]),TmpEleProjCnt);
        logCPSratio = cps.computeLogRatio(ri, rj, s) ;
        if ( usePairProd ) {
        	w = exp(2.0*(x + logCPSratio + logIpNew-logIpOld));
        } else {
        	w = exp(2.0*(x + logCPSratio));
        }

        if( !isfinite(w) ) w = -1.0; /* should be rejected */

        if(w > genrand_real2()) { /* accept */
          StartTimer(63);
          UpdateMAll(mi,s,TmpEleIdx,qpStart,qpEnd);
          StopTimer(63);

          for(i=0;i<NProj;i++) TmpEleProjCnt[i] = projCntNew[i];
          cps.update() ;
          logIpOld = logIpNew;
          nAccept++;
          Counter[1]++;
        } else { /* reject */
          TmpEleIdx[mi+s*Ne] = ri;
          TmpEleCfg[ri+s*Nsite] = mi;
          TmpEleCfg[rj+s*Nsite] = -1;
          TmpEleNum[ri+s*Nsite] = 1;
          TmpEleNum[rj+s*Nsite] = 0;
        }
        Counter[0]++;
        StopTimer(32);
      } else { /* exchange */
        StartTimer(33);
        StartTimer(65);
        /* The mi-th electron with spin s exchanges with the electron on site rj with spin 1-s */
        t = 1-s;
        mj = TmpEleCfg[rj+t*Nsite];
        /* The mi-th electron with spin s hops to rj */
        TmpEleIdx[mi+s*Ne] = rj;
        TmpEleCfg[ri+s*Nsite] = -1;
        TmpEleCfg[rj+s*Nsite] = mi;
        TmpEleNum[ri+s*Nsite] = 0;
        TmpEleNum[rj+s*Nsite] = 1;
        UpdateProjCnt(ri,rj,s,&(projCntNew[0]),TmpEleProjCnt,TmpEleNum);
        /* The mj-th electron with spin t hops to ri */
        TmpEleIdx[mj+t*Ne] = ri;
        TmpEleCfg[rj+t*Nsite] = -1;
        TmpEleCfg[ri+t*Nsite] = mj;
        TmpEleNum[rj+t*Nsite] = 0;
        TmpEleNum[ri+t*Nsite] = 1;
        UpdateProjCnt(rj,ri,t,&(projCntNew[0]),&(projCntNew[0]),TmpEleNum);

        StopTimer(65);
        StartTimer(66);

        CalculateNewPfMTwo2(mi, s, mj, t, &(pfMNew[0]), TmpEleIdx, qpStart, qpEnd);

        StopTimer(66);
        StartTimer(67);

        /* calculate inner product <phi|L|x> */
        logIpNew = CalculateLogIP(&(pfMNew[0]),qpStart,qpEnd,comm);

        StopTimer(67);

        /* Metroplis */
        x = LogProjRatio(&(projCntNew[0]),TmpEleProjCnt);
        logCPSratio = cps.computeLogRatio(ri, rj, s, rj, ri, t) ;
        if ( usePairProd ) {
        	w = exp(2.0*(x + logCPSratio + logIpNew-logIpOld));
        } else {
        	w = exp(2.0*(x + logCPSratio));
        }
        if( !isfinite(w) ) w = -1.0; /* should be rejected */

        if(w > genrand_real2()) { /* accept */
          StartTimer(68);
          UpdateMAllTwo(mi, s, mj, t, ri, rj, TmpEleIdx,qpStart,qpEnd);
          StopTimer(68);

/*           printf("%d %lf %lf\n",inStep,InvM[(mi+s*Ne)*Nsize+mj+t*Ne],InvM[(mi+s*Ne)*Nsize+mj+t*Ne+1]); */
/*           CalculateMAll(TmpEleIdx,qpStart,qpEnd); */
/*           printf("%d %lf %lf\n",inStep,InvM[(mi+s*Ne)*Nsize+mj+t*Ne],InvM[(mi+s*Ne)*Nsize+mj+t*Ne+1]); */

          for(i=0;i<NProj;i++) TmpEleProjCnt[i] = projCntNew[i];
          cps.update() ;
          logIpOld = logIpNew;
          nAccept++;
          Counter[3]++;
        } else { /* reject */
          TmpEleIdx[mj+t*Ne] = rj;
          TmpEleCfg[rj+t*Nsite] = mj;
          TmpEleCfg[ri+t*Nsite] = -1;
          TmpEleNum[rj+t*Nsite] = 1;
          TmpEleNum[ri+t*Nsite] = 0;

          TmpEleIdx[mi+s*Ne] = ri;
          TmpEleCfg[ri+s*Nsite] = mi;
          TmpEleCfg[rj+s*Nsite] = -1;
          TmpEleNum[ri+s*Nsite] = 1;
          TmpEleNum[rj+s*Nsite] = 0;
        }
        Counter[2]++;
        StopTimer(33);
      }

      if(nAccept>Nsite) {
        StartTimer(34);
        /* recal PfM and InvM */
        CalculateMAll(TmpEleIdx,qpStart,qpEnd);
        logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);
        StopTimer(34);
        nAccept=0;
      }
    } /* end of instep */

    StartTimer(35);
    /* save Electron Configuration */
    if(outStep >= nOutStep-NVMCSample) {
      sample = outStep-(nOutStep-NVMCSample);
      saveEleConfig(sample,logIpOld,TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
    }
    StopTimer(35);

  } /* end of outstep */

  copyToBurnSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
  BurnFlag=1;
  return;
}

void VMCMakeSample(MPI_Comm comm_parent, MPI_Comm comm, FatTreeTensorNetwork& fttn) {
  int outStep,nOutStep;
  int inStep,nInStep;
  UpdateType updateType;
  int mi,mj,ri,rj,s,t,i;
  int nAccept=0, nAccept_hop = 0, nAccept_exc = 0 ;
  int PfRecal_hop = Nsite, PfRecal_exc = Nsite ;
  int sample;

  double logIpOld,logIpNew; /* logarithm of inner product <phi|L|x> */
  vector<int> projCntNew(NProj, 0) ;
  vector<double> pfMNew(NQPFull, 0) ;
  double x,w;
  double logFTTNratio ;

  int qpStart,qpEnd;
  int rejectFlag ;
  int rank,size;
  MPI_Comm_size(comm,&size);
  MPI_Comm_rank(comm,&rank);

  SplitLoop(&qpStart,&qpEnd,NQPFull,rank,size);

  StartTimer(30);
  if(BurnFlag==0) {
  	PfRecal = Nsite ;
    makeInitialSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt,
                      qpStart,qpEnd,comm);
  } else {
    copyFromBurnSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
  }

  replaceBadConditionSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt, qpStart, qpEnd, comm_parent) ;

  int info = CalculateMAll(TmpEleIdx,qpStart,qpEnd);
  logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);
//  if ( info != 0 ) {
  if ( !isfinite(logIpOld) ) {
  	if(rank==0) fprintf(stderr,"warning: VMCMakeSample remakeSample logIpOld=%e\n",logIpOld);
  	makeInitialSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt,
  	                  qpStart,qpEnd,comm);
    CalculateMAll(TmpEleIdx,qpStart,qpEnd);
    logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);
  	BurnFlag = 0;
  }
  fttn.initialize(TmpEleNum) ;
  StopTimer(30);

  nOutStep = (BurnFlag==0) ? NVMCWarmUp+NVMCSample : NVMCSample+1;
  nInStep = NVMCIniterval * Nsite;

  for(i=0;i<4;i++) Counter[i]=0;  /* reset counter */

  for(outStep=0;outStep<nOutStep;outStep++) {
    for(inStep=0;inStep<nInStep;inStep++) {

    	updateType = getUpdateType(NExUpdatePath);
    	if ( updateType==HOPPING )  { // hopping
    		Counter[0]++;
    		StartTimer(31);
        makeCandidate_hopping(&mi, &ri, &rj, &s, &rejectFlag,
                              TmpEleIdx, TmpEleCfg);
        StopTimer(31);
        if(rejectFlag) continue;

        StartTimer(32);
        	StartTimer(60);
        	/* The mi-th electron with spin s hops to site rj */
        updateEleConfig(mi,ri,rj,s,TmpEleIdx,TmpEleCfg,TmpEleNum);
        UpdateProjCnt(ri,rj,s,&(projCntNew[0]),TmpEleProjCnt,TmpEleNum);
        	StopTimer(60);
        	StartTimer(61);
        CalculateNewPfM2(mi,s,&(pfMNew[0]),TmpEleIdx,qpStart,qpEnd);
        	StopTimer(61);

        	StartTimer(62);
        // calculate inner product <phi|L|x>
        logIpNew = CalculateLogIP(&(pfMNew[0]),qpStart,qpEnd,comm);
        	StopTimer(62);
        // Metroplis
        x = LogProjRatio(&(projCntNew[0]),TmpEleProjCnt);
      		StartTimer(64);
        logFTTNratio = fttn.computeLogRatio(ri, rj, s) ;

      		StopTimer(64);
        if ( usePairProd ) {
          w = exp( 2.0*(x + logFTTNratio + logIpNew-logIpOld) );
        } else {
          w = exp( 2.0*(x + logFTTNratio) ) ;
        }
        if( !isfinite(w) ) w = -1.0; /* should be rejected */

        if(w > genrand_real2()) { /* accept */
          StartTimer(63);
          UpdateMAll(mi,s,TmpEleIdx,qpStart,qpEnd);
          StopTimer(63);

          for(i=0;i<NProj;i++) TmpEleProjCnt[i] = projCntNew[i];
          fttn.update() ;
          logIpOld = logIpNew;
          nAccept++;
          nAccept_hop++ ;
          Counter[1]++;
        } else { /* reject */
        	revertEleConfig(mi,ri,rj,s,TmpEleIdx,TmpEleCfg,TmpEleNum);
        }
        StopTimer(32);
    	} else if (updateType==EXCHANGE) { /* exchange */
    		Counter[2]++;
        StartTimer(31);
        makeCandidate_exchange(&mi, &ri, &rj, &s, &rejectFlag,
                               TmpEleIdx, TmpEleCfg, TmpEleNum);
        StopTimer(31);
        if(rejectFlag) continue;
        StartTimer(33);
        StartTimer(65);
        /* The mi-th electron with spin s exchanges with the electron on site rj with spin 1-s */
        t = 1-s;
        mj = TmpEleCfg[rj+t*Nsite];
        /* The mi-th electron with spin s hops to rj */
        updateEleConfig(mi,ri,rj,s,TmpEleIdx,TmpEleCfg,TmpEleNum);
        UpdateProjCnt(ri,rj,s,&(projCntNew[0]),TmpEleProjCnt,TmpEleNum);
        /* The mj-th electron with spin t hops to ri */
        updateEleConfig(mj,rj,ri,t,TmpEleIdx,TmpEleCfg,TmpEleNum);
        UpdateProjCnt(rj,ri,t,&(projCntNew[0]),&(projCntNew[0]),TmpEleNum);
        StopTimer(65);
        StartTimer(66);
        CalculateNewPfMTwo2(mi, s, mj, t, &(pfMNew[0]), TmpEleIdx, qpStart, qpEnd);
        StopTimer(66);
        StartTimer(67);
        /* calculate inner product <phi|L|x> */
        logIpNew = CalculateLogIP(&(pfMNew[0]),qpStart,qpEnd,comm);
        StopTimer(67);
        // Metroplis
        x = LogProjRatio(&(projCntNew[0]),TmpEleProjCnt);
        StartTimer(69);
        logFTTNratio = fttn.computeLogRatio(ri, rj, s, rj, ri, t) ;
        StopTimer(69);
        if ( usePairProd ) {
        	w = exp(2.0*(x + logFTTNratio + logIpNew-logIpOld));
        } else {
        	w = exp( 2.0*(x + logFTTNratio) ) ;
        }
        if( !isfinite(w) ) w = -1.0; /* should be rejected */

        if(w > genrand_real2()) { /* accept */
          StartTimer(68);
          UpdateMAllTwo(mi, s, mj, t, ri, rj, TmpEleIdx,qpStart,qpEnd);
          StopTimer(68);
          for(i=0;i<NProj;i++) TmpEleProjCnt[i] = projCntNew[i];
          fttn.update() ;
          logIpOld = logIpNew;
          nAccept ++;
          nAccept_exc ++ ;
          Counter[3]++;
        } else { /* reject */
          revertEleConfig(mj,rj,ri,t,TmpEleIdx,TmpEleCfg,TmpEleNum);
          revertEleConfig(mi,ri,rj,s,TmpEleIdx,TmpEleCfg,TmpEleNum);
        }
        StopTimer(33);
    	} // end of exchange

      if(nAccept>PfRecal) {
        StartTimer(34);
        logIpNew = logIpOld ;
        /* recal PfM and InvM */
        CalculateMAll(TmpEleIdx,qpStart,qpEnd);
        logIpOld = CalculateLogIP(PfM,qpStart,qpEnd,comm);
        StopTimer(34);
        nAccept=0;
      }
    } /* end of instep */
    StartTimer(35);
    /* save Electron Configuration */
    if(outStep >= nOutStep-NVMCSample) {
      sample = outStep-(nOutStep-NVMCSample);
      saveEleConfig(sample,logIpOld,TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
    }
    StopTimer(35);
    //--------------------------------------------------
  } /* end of outstep */
  copyToBurnSample(TmpEleIdx,TmpEleCfg,TmpEleNum,TmpEleProjCnt);
  BurnFlag=1;

  return;
}

// eleIdx[2*Ne] eleCfg[2*Nsite]
int makeInitialSample(int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt,
                      const int qpStart, const int qpEnd, MPI_Comm comm) {
  const int nsize = Nsize;
  const int nsite2 = Nsite2;
  int flag=1,flagRdc,loop=0;
  int ri,mi,si,msi,rsi;
  int rank,size;
  MPI_Comm_size(comm,&size);
  MPI_Comm_rank(comm,&rank);

  do {
    /* initialize */
    #pragma omp parallel for default(shared) private(msi)
    for(msi=0;msi<nsize;msi++) eleIdx[msi] = -1;
    #pragma omp parallel for default(shared) private(rsi)
    for(rsi=0;rsi<nsite2;rsi++) eleCfg[rsi] = -1;
    
    /* local spin */
    for(ri=0;ri<Nsite;ri++) {
      if(LocSpn[ri]==0) {
        do {
          mi = gen_rand32()%Ne;
          si = (genrand_real2()<0.5) ? 0 : 1;
        } while(eleIdx[mi+si*Ne]!=-1);
        eleCfg[ri+si*Nsite] = mi;
        eleIdx[mi+si*Ne] = ri;
      }
    }
    
    /* itinerant electron */
    for(si=0;si<2;si++) { // spin
      for(mi=0;mi<Ne;mi++) { // electron
        if(eleIdx[mi+si*Ne]== -1) {
          do {
            ri = gen_rand32()%Nsite;
          } while (eleCfg[ri+si*Nsite]!= -1 || LocSpn[ri]==0);
          eleCfg[ri+si*Nsite] = mi;
          eleIdx[mi+si*Ne] = ri;
        }
      }
    }
    
    /* EleNum */
    #pragma omp parallel for default(shared) private(rsi)
    #pragma loop noalias
    for(rsi=0;rsi<nsite2;rsi++) {
      eleNum[rsi] = (eleCfg[rsi] < 0) ? 0 : 1;
    }
    //----------------------------------------------------------
    MakeProjCnt(eleProjCnt,eleNum);
    if ( NSlater > 0 ) {
    	flag = CalculateMAll(eleIdx,qpStart,qpEnd);
    }
    else flag = 0 ;

    if(size>1) {
      MPI_Allreduce(&flag,&flagRdc,1,MPI_INT,MPI_MAX,comm);
      flag = flagRdc;
    }
    loop++;
    if(loop>1000) {
      if(rank==0) fprintf(stderr, "error: makeInitialSample: Too many loops\n");
      MPI_Abort(MPI_COMM_WORLD,EXIT_FAILURE);
    }
  } while (flag != 0) ;
  return 0;
}

void copyFromBurnSample(int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt) {
  int i,n;
  const int *burnEleIdx = BurnEleIdx;
  n = Nsize + 2*Nsite + 2*Nsite + NProj;
  #pragma loop noalias
  for(i=0;i<n;i++) eleIdx[i] = burnEleIdx[i];
  return;
}

void copyToBurnSample(const int *eleIdx, const int *eleCfg,
		const int *eleNum, const int *eleProjCnt) {
  int i,n;
  int *burnEleIdx = BurnEleIdx;
  n = Nsize + 2*Nsite + 2*Nsite + NProj;
  #pragma loop noalias
  for(i=0;i<n;i++) burnEleIdx[i] = eleIdx[i];
  return;
}

void saveEleConfig(const int sample, const double logIp,
                   const int *eleIdx, const int *eleCfg, const int *eleNum, const int *eleProjCnt) {
  int i,offset;
  double x;
  const int nsize=Nsize;
  const int nsite2 = Nsite2;
  const int nProj = NProj;

  offset = sample*nsize;
  #pragma loop noalias
  for(i=0;i<nsize;i++) EleIdx[offset+i] = eleIdx[i];
  offset = sample*nsite2;
  #pragma loop noalias
  for(i=0;i<nsite2;i++) EleCfg[offset+i] = eleCfg[i];
  #pragma loop noalias
  for(i=0;i<nsite2;i++) EleNum[offset+i] = eleNum[i];
  offset = sample*nProj;
  #pragma loop noalias
  for(i=0;i<nProj;i++) EleProjCnt[offset+i] = eleProjCnt[i];
  
  x = LogProjVal(eleProjCnt);
  logSqPfFullSlater[sample] = 2.0*(x+logIp);
  
  return;
}

void sortEleConfig(int *eleIdx, int *eleCfg, const int *eleNum) {
/*   int ri,mi=0; */
/*   for(ri=0;ri<Nsite;ri++) { */
/*     if(eleNum[ri]>0) { */
/*       eleCfg[ri]=mi; */
/*       eleIdx[mi]=ri; */
/*       mi++; */
/*     } else { */
/*       eleCfg[ri]=-1; */
/*     } */
/*   } */
/*   mi=0; */
/*   for(ri=0;ri<Nsite;ri++) { */
/*     if(eleNum[ri+Nsite]>0) { */
/*       eleCfg[ri+Nsite]=mi; */
/*       eleIdx[mi+Ne]=ri; */
/*       mi++; */
/*     } else { */
/*       eleCfg[ri+Nsite]=-1; */
/*     } */
/*   } */

  return;
}

void ReduceCounter(MPI_Comm comm) {
  #ifdef _mpi_use
  const int n=4;
  int recv[n];
  int i;
  int rank,size;
  MPI_Comm_size(comm,&size);
  MPI_Comm_rank(comm,&rank);

  MPI_Allreduce(Counter,recv,n,MPI_INT,MPI_SUM,comm);
  if(rank==0) {
    for(i=0;i<n;i++) Counter[i] = recv[i];
  }
  #endif
  return;
}

/* The mi-th electron with spin s hops to site rj */
void makeCandidate_hopping(int *mi_, int *ri_, int *rj_, int *s_, int *rejectFlag_,
                           const int *eleIdx, const int *eleCfg) {
  const int icnt_max = Nsite*Nsite;
  int icnt;
  int mi, ri, rj, s, flag;

  flag = 0; // FALSE
  do {
    mi = gen_rand32()%Ne;
    s = (genrand_real2()<0.5) ? 0 : 1;
    ri = eleIdx[mi+s*Ne];
  } while (LocSpn[ri] == 0);

  icnt = 0;
  do {
    rj = gen_rand32()%Nsite;
    if(icnt> icnt_max){
      flag = 1; // TRUE
      break;
    }
    icnt+=1;
  } while (eleCfg[rj+s*Nsite] != -1 || LocSpn[rj]==0);

  *mi_ = mi;
  *ri_ = ri;
  *rj_ = rj;
  *s_ = s;
  *rejectFlag_ = flag;

  return;
}

/* The mi-th electron with spin s exchanges with the electron on site rj with spin 1-s */
void makeCandidate_exchange(int *mi_, int *ri_, int *rj_, int *s_, int *rejectFlag_,
                           const int *eleIdx, const int *eleCfg, const int *eleNum) {
  int mi, mj, ri, rj, s, t, flag;

  flag = 1; // TRUE
  for(ri=0;ri<Nsite;ri++){
    if((eleNum[ri]+eleNum[ri+Nsite]) == 1){ // single occupied site
      flag = 0; // FALSE: find single occupied site
      break;
    }
  }
  if(flag) { // if no single occupied site, reject all move
    *rejectFlag_ = flag;
    return;
  }

  do {
    mi = gen_rand32()%Ne; // mi-th electron
    s = (genrand_real2()<0.5) ? 0 : 1;
    ri = eleIdx[mi+s*Ne];
  } while (eleCfg[ri+(1-s)*Nsite] != -1);
  do {
    mj = gen_rand32()%Ne;
    t = 1-s;
    rj = eleIdx[mj+t*Ne];
  } while (eleCfg[rj+(1-t)*Nsite] != -1);

  *mi_ = mi;
  *ri_ = ri;
  *rj_ = rj;
  *s_ = s;
  *rejectFlag_ = flag;

  return;
}

/* The mi-th electron with spin s hops to site rj */
void updateEleConfig(int mi, int ri, int rj, int s,
                     int *eleIdx, int *eleCfg, int *eleNum) {
  eleIdx[mi+s*Ne] = rj;
  eleCfg[ri+s*Nsite] = -1;
  eleCfg[rj+s*Nsite] = mi;
  eleNum[ri+s*Nsite] = 0;
  eleNum[rj+s*Nsite] = 1;
  return;
}

void revertEleConfig(int mi, int ri, int rj, int s,
                     int *eleIdx, int *eleCfg, int *eleNum) {
  eleIdx[mi+s*Ne] = ri;
  eleCfg[ri+s*Nsite] = mi;
  eleCfg[rj+s*Nsite] = -1;
  eleNum[ri+s*Nsite] = 1;
  eleNum[rj+s*Nsite] = 0;
  return;
}


UpdateType getUpdateType(int path) {
  if(path==0) {
    return HOPPING;
  } else if (path==1) {
    return (genrand_real2()<0.5) ? EXCHANGE : HOPPING; /* exchange or hopping */
  } else if (path==2) {
    return EXCHANGE;
  }
  return NONE;
}

double find_array_max(const double* a, const int len) {
	double amax = a[0] ;
	for ( int i = 1; i < len; i ++ ) {
		if ( a[i] > amax ) amax = a[i] ;
	}
	return amax ;
}

double find_array_min(const double* a, const int len, int& minIdx) {
	double amin = a[0] ;
	minIdx = 0 ;
	for ( int i = 1; i < len; i ++ ) {
		if ( a[i] < amin ) {
			amin = a[i] ;
			minIdx = i ;
		}
	}
	return amin ;
}

void replaceBadConditionSample(int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt,
														   const int qpStart, const int qpEnd, MPI_Comm comm) {
  int rank,size;
  MPI_Comm_size(comm,&size);
  MPI_Comm_rank(comm,&rank);
	CalculateMAll(eleIdx,qpStart,qpEnd);
	const int qpNum = qpEnd-qpStart;
	double max_condNum = find_array_max(McondNum, qpNum) ;

	vector<double> max_condNum_all(size, 0.0) ;
	MPI_Allgather(&max_condNum, 1, MPI_DOUBLE, &(max_condNum_all[0]), 1, MPI_DOUBLE, comm) ;

	int minRank ;
	find_array_min(&(max_condNum_all[0]), size, minRank) ;
	// broadcast minRank sample to all other rank
	// create buffers
	vector<int> eleIdx_buf(eleIdx, eleIdx + Nsize) ;
	vector<int> eleCfg_buf(eleCfg, eleCfg + Nsite2) ;
	vector<int> eleNum_buf(eleNum, eleNum + Nsite2) ;
	vector<int> eleProjCnt_buf(eleProjCnt, eleProjCnt + NProj) ;
	MPI_Bcast(&(eleIdx_buf[0]), Nsize, MPI_INT, minRank, comm) ;
	MPI_Bcast(&(eleCfg_buf[0]), Nsite2, MPI_INT, minRank, comm) ;
	MPI_Bcast(&(eleNum_buf[0]), Nsite2, MPI_INT, minRank, comm) ;
	MPI_Bcast(&(eleProjCnt_buf[0]), NProj, MPI_INT, minRank, comm) ;
	//----------------------------------------------------
	if ( max_condNum > condNum_thresh ) {
		for ( int i = 0; i < eleIdx_buf.size(); i ++ )
			eleIdx[i] = eleIdx_buf[i] ;
		for ( int i = 0; i < eleCfg_buf.size(); i ++ )
			eleCfg[i] = eleCfg_buf[i] ;
		for ( int i = 0; i < eleNum_buf.size(); i ++ )
			eleNum[i] = eleNum_buf[i] ;
		for ( int i = 0; i < eleProjCnt_buf.size(); i ++ )
			eleProjCnt[i] = eleProjCnt_buf[i] ;
	}
	//---------------------------------------------------------
	int NbadSample = 0 ;
	for ( int i = 0; i < size; i ++ ) {
		if ( max_condNum_all[i] > condNum_thresh ) NbadSample ++ ;
	}
	double replaceRatio = (double)NbadSample / (double)size ;
	if ( rank == 0 && replaceRatio != 0) {
		cout << "replace initial sample ratio: " << replaceRatio << endl ;
	}
	//---------------------------------------------------------
//	CalculateMAll(eleIdx,qpStart,qpEnd);
//	max_condNum = find_array_max(McondNum, qpNum) ;
//	MPI_Allgather(&max_condNum, 1, MPI_DOUBLE, &(max_condNum_all[0]), 1, MPI_DOUBLE, comm) ;
//	if ( rank == 0 ){
//		for ( int i = 0; i < size; i ++ ) {
//			cout << max_condNum_all[i] << "; " ;
//		}
//		cout << endl ;
//		MPI_Abort(comm, 0);
//	}
}
