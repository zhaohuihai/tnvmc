/*-------------------------------------------------------------
 * Variational Monte Carlo
 * calculate weighted averages of physical quantities
 *-------------------------------------------------------------
 * by Satoshi Morita
 *-------------------------------------------------------------*/
#include <vector>

#include "vmcmain.h"

#include "safempi.h"
#include "workspace.h"

#include "average.h"

using namespace std ;

/* calculate average of Wc, Etot and Etot2 */
/* All processes will have the result */
void WeightAverageWE(MPI_Comm comm) {
  const int n=4;
  double invW;
  int rank,size;
  double send[n], recv[n];
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

  /* Wc, Etot and Etot2 */
  if(size>1) {
    send[0] = Wc;
    send[1] = Etot;
    send[2] = Etot2;
    send[3] = Etot1 ;
    SafeMpiAllReduce(send,recv,n,comm);

    Wc    = recv[0];
    invW  = 1.0/Wc;
    Etot  = recv[1]*invW;
    Etot2 = recv[2]*invW;
  } else {
    invW  = 1.0/Wc;
    Etot  *= invW;
    Etot2 *= invW;
  }

  return;
}

/* calculate average of QQQQ */
/* All processes will have the result */
void WeightAverageLanczos(MPI_Comm comm) {
	int i ;
	int n = NLSHam*NLSHam*NLSHam*NLSHam;
	double invW = 1.0/Wc;
	double *vec = QQQQ ;
	double *buf ;
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);
  if ( size > 1 ) {
//  	std::vector<double> buf(n, -1) ;
  	RequestWorkSpaceDouble(n);
  	buf = GetWorkSpaceDouble(n);
  	SafeMpiAllReduce(vec, buf, n, comm) ;
//		#pragma omp parallel for default(shared) private(i)
//		#pragma loop noalias
  	for(i=0;i<n;i++) vec[i] = buf[i] * invW;
  	ReleaseWorkSpaceDouble();
  } else {
  	for(i=0;i<n;i++) vec[i] *= invW;
  }

}

/* calculate average of SROptOO and SROptHO */
/* All processes will have the result */

//void WeightAverageSROpt(MPI_Comm comm) {
//  int i,n;
//  double invW = 1.0/Wc;
//  double *vec,*buf;
//  int rank,size;
//  MPI_Comm_rank(comm,&rank);
//  MPI_Comm_size(comm,&size);
//
//  if ( useSRCG ) {
////  	computeAveSD(comm, SROptSize, SROptO, SROptO_SD, NVMCSample) ;
//
//  	n = SROptSize ;
//  	vec = SROptO ;
//  	if (size > 1) {
//  		RequestWorkSpaceDouble(n);
//  		buf = GetWorkSpaceDouble(n);
//  		SafeMpiAllReduce(vec,buf,n,comm);
//			#pragma omp parallel for default(shared) private(i)
//			#pragma loop noalias
//  		for(i=0;i<n;i++) vec[i] = buf[i] * invW;
//  		ReleaseWorkSpaceDouble();
//  	} else {
//			#pragma omp parallel for default(shared) private(i)
//			#pragma loop noalias
//  		for(i=0;i<n;i++) vec[i] *= invW;
//  	}
//
//  	if ( rank == 0 ) {
//  		for(i = 0 ; i < SROptSize; i ++) cout << SROptO[i] << ", " << SROptO_SD[i] << endl ;
//  	}
//  	MPI_Barrier(comm) ;
//  	MPI_Abort(comm, 1) ;
//
//  	//==================================================
//  	// SROptHO
//  	n = SROptSize;
//  	vec = SROptHO;
//  	if(size>1) {
//  		RequestWorkSpaceDouble(n);
//  		buf = GetWorkSpaceDouble(n);
//  		SafeMpiAllReduce(vec,buf,n,comm);
//			#pragma omp parallel for default(shared) private(i)
//			#pragma loop noalias
//  		for(i=0;i<n;i++) vec[i] = buf[i] * invW;
//  		ReleaseWorkSpaceDouble();
//  	} else {
//			#pragma omp parallel for default(shared) private(i)
//			#pragma loop noalias
//  		for(i=0;i<n;i++) vec[i] *= invW;
//  	}
//  	//==================================================
//  	// SROptOOdiag
//  	n = SROptSize ;
//  	vec = SROptOOdiag ;
//  	if(size > 1) {
//  		RequestWorkSpaceDouble(n);
//  		buf = GetWorkSpaceDouble(n);
//  		SafeMpiAllReduce(vec,buf,n,comm);
//			#pragma omp parallel for default(shared) private(i)
//			#pragma loop noalias
//  		for(i=0;i<n;i++) vec[i] = buf[i] * invW;
//  		ReleaseWorkSpaceDouble();
//  	} else {
//			#pragma omp parallel for default(shared) private(i)
//			#pragma loop noalias
//  		for(i=0;i<n;i++) vec[i] *= invW;
//  	}
//  }
//  else { // not use CG
//  	/* SROptOO and SROptHO */
//  	n = SROptSize*(SROptSize+1);
//  	vec = SROptOO;
//  	if(size>1) {
//  		RequestWorkSpaceDouble(n);
//  		buf = GetWorkSpaceDouble(n);
//
//  		SafeMpiAllReduce(vec,buf,n,comm);
//
//			#pragma omp parallel for default(shared) private(i)
//			#pragma loop noalias
//  		for(i=0;i<n;i++) vec[i] = buf[i] * invW;
//
//  		ReleaseWorkSpaceDouble();
//  	} else {
//			#pragma omp parallel for default(shared) private(i)
//			#pragma loop noalias
//  		for(i=0;i<n;i++) vec[i] *= invW;
//  	}
//  }
//  //================debug=========================
////  if (rank == 0) {
////    for ( i = 1; i < NProj + 1; i ++) {
////    	cout << i << ": " << SROptO[i] << endl ;
////    }
////    cout << endl ;
////    for ( i = NProj + 1; i < NProj+NSlater + 1; i ++) {
////    	std::cout << i << ": " << SROptO[i] << std::endl ;
////    }
////    std::cout << std::endl << std::endl ;
////    for ( i = NProj+NSlater + 1; i < NProj+NSlater+NPEPSelem+ 1; i ++) std::cout << SROptO[i] << ", " ;
////  }
////  MPI_Barrier(comm) ;
////  exit(0) ;
//  return;
//}

void WeightAverageSROpt(MPI_Comm comm) {
  int i,n;
  double invW = 1.0/Wc;
  double *vec,*buf;
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

  if ( useSRCG ) {
  	computeAveSD(comm, SROptSize, SROptO, SROptO_SD, NVMCSample) ;
  	computeAve(comm, SROptSize, SROptOOdiag, NVMCSample) ;
  	computeAve(comm, SROptSize, SROptOOOdiag, NVMCSample) ;
  	computeAve(comm, SROptSize, SROptOOOOdiag, NVMCSample) ;
  	//==================================================
  	computeAveSD(comm, SROptSize, SROptHO, SROptHO_SD, NVMCSample) ;
  	//==================================================

//  	if ( rank == 0 ) {
//  		for(i = 0 ; i < SROptSize; i ++)
//  			cout << SROptOOdiag[i] << ", " << SROptOOdiag_SD[i] << endl ;
//  	//  		for(i = 0 ; i < SROptSize; i ++) cout << i << ": " << SROptHO[i] << ", " << SROptHO_SD[i] << endl ;
//  	}
  }
  else { // not use CG
  	/* SROptOO and SROptHO */
  	n = SROptSize*(SROptSize+1);
  	vec = SROptOO;
  	if(size>1) {
  		RequestWorkSpaceDouble(n);
  		buf = GetWorkSpaceDouble(n);

  		SafeMpiAllReduce(vec,buf,n,comm);

			#pragma omp parallel for default(shared) private(i)
			#pragma loop noalias
  		for(i=0;i<n;i++) vec[i] = buf[i] * invW;

  		ReleaseWorkSpaceDouble();
  	} else {
			#pragma omp parallel for default(shared) private(i)
			#pragma loop noalias
  		for(i=0;i<n;i++) vec[i] *= invW;
  	}
  }
  //================debug=========================
//  if (rank == 0) {
//    for ( i = 1; i < NProj + 1; i ++) {
//    	cout << i << ": " << SROptO[i] << endl ;
//    }
//    cout << endl ;
//    for ( i = NProj + 1; i < NProj+NSlater + 1; i ++) {
//    	std::cout << i << ": " << SROptO[i] << std::endl ;
//    }
//    std::cout << std::endl << std::endl ;
//    for ( i = NProj+NSlater + 1; i < NProj+NSlater+NPEPSelem+ 1; i ++) std::cout << SROptO[i] << ", " ;
//  }
//  MPI_Barrier(comm) ;
//  exit(0) ;
  return;
}

void computeAve(MPI_Comm comm, int n, double *data, int nsample) {
	int rank, size ;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

  vector<double> data_mean(n) ;
  for ( int i = 0; i < n; i ++ ) data[i] /= (double)nsample ;
  if ( size > 1 ) {
  	SafeMpiAllReduce(data, &(data_mean[0]), n, comm) ;
  	for ( int i = 0; i < n; i ++ ) {
  		data_mean[i] /= (double)size ;
  		data[i] = data_mean[i] ;
  	}
  }
}

void computeAveSD(MPI_Comm comm, int n, double *data, double *data_SD, int nsample) {
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

  double date_proc_SD ;

  vector<double> data_mean(n) ;
  for ( int i = 0; i < n; i ++ ) data[i] /= (double)nsample ;
  if ( size > 1 ) {
  	SafeMpiAllReduce(data, &(data_mean[0]), n, comm) ;
  	for ( int i = 0; i < n; i ++ ) data_mean[i] /= (double)size ;

  	for ( int i = 0; i < n; i ++ ) {
  		data_SD[i] = 0.0 ;
  		date_proc_SD = data[i] - data_mean[i] ;
  		date_proc_SD = date_proc_SD * date_proc_SD ;
  		SafeMpiAllReduce(&date_proc_SD, &(data_SD[i]), 1, comm) ;
  		data_SD[i] = sqrt(data_SD[i]) / (double)size ;
  		data[i] = data_mean[i] ;
  	}
  } else { // size == 1, date_SD is 0
  	for ( int i = 0; i < n; i ++ ) data_SD[i] = 0.0 ;
  }
}


/* calculate average of Green functions */
/* Only rank=0 process will have the result */
void WeightAverageGreenFunc(MPI_Comm comm) {
  int n;
  double *vec;

  /* Green functions */
  /* CisAjs, CisAjsCktAlt and CisAjsCktAltDC */
  n = NCisAjs+NCisAjsCktAlt+NCisAjsCktAltDC;
  vec = PhysCisAjs;
  weightAverageReduce(n,vec,comm);
  
  if(NLanczosMode>0){
    /* QQQQ */
    n = NLSHam*NLSHam*NLSHam*NLSHam;
    vec = QQQQ;
    weightAverageReduce(n,vec,comm);
    
    if(NLanczosMode>1){
      /* QCisAjsQ and QCisAjsCktAltQ */
      n = NLSHam*NLSHam*NCisAjs + NLSHam*NLSHam*NCisAjsCktAlt;
      vec = QCisAjsQ;
      weightAverageReduce(n,vec,comm);
    }
  }  

  return;
}

void weightAverageReduce(int n, double *vec, MPI_Comm comm) {
  int i;
  const double invW = 1.0/Wc;
  double *buf;
  int rank,size;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

  if(size>1) {
    RequestWorkSpaceDouble(n);
    buf = GetWorkSpaceDouble(n);

    SafeMpiReduce(vec,buf,n,comm);
    if(rank==0) {
      #pragma omp parallel for default(shared) private(i)
      #pragma loop noalias
      for(i=0;i<n;i++) vec[i] = buf[i] * invW;
    }

    ReleaseWorkSpaceDouble();
  } else {
    #pragma omp parallel for default(shared) private(i)
    #pragma loop noalias
    for(i=0;i<n;i++) vec[i] *= invW;
  }

  return;
}
