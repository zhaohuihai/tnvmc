/*
 * slater.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_SLATER_H_
#define SRC_SLATER_H_

#include <vector>

void UpdateSlaterElm();
void SlaterElmDiff(double *srOptO, const double ip, int *eleIdx);

void computePfDerQPfull(std::vector<double>& PfDerQPfull, int *eleIdx) ;

#endif /* SRC_SLATER_H_ */
