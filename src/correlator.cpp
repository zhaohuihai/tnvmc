/*
 * correlator.cpp
 *
 *  Created on: 2016-1-15
 *      Author: zhaohuihai
 */

#include <math.h>
#include <stdlib.h>
#include "blas_lapack.hpp"
#include "correlator.h"

using namespace std ;

Correlator::Correlator(const int nsites, const std::vector<int>& site, const int siteDim, double* elem)
{
	_ti = false ;
	_siteDim = siteDim ;
	_site = site ;
	_nsites = nsites ;
	_elem = elem ;

	_nelements = pow((double)_siteDim, (double)_site.size()) ;
}

double Correlator::compute_weight(const int* eleNum)
{
	int physInd ;
	int idx = 0 ;
	int base = 1 ;
	for ( int i = 0; i < _site.size(); i ++ ) {
		physInd = getPhysInd(eleNum, i) ;
		idx += physInd * base ;
		base *= _siteDim ;
	}

	return _elem[idx] ;
}

double Correlator::compute_weight(const std::vector<int>& eleNum)
{
	return compute_weight(&eleNum.at(0)) ;
}

int Correlator::getPhysInd(const int* eleNum, const int i)
{
	int ilattice = _site.at(i) ; // find site index in the lattice
//	std::cout << "ilattice: " << ilattice << std::endl ;
	int physInd ;
	int upInd, downInd ;
	upInd = eleNum[ilattice] ;
	downInd = eleNum[ilattice + _nsites] ;
	physInd = upInd + downInd * 2 ;
//	std::cout << "physInd: " << physInd << std::endl ;
	if ( physInd < 0 || physInd >= _siteDim)
	{
		std::cout << "MatrixProductState<SCALAR>::getPhysInd error: " ;
		std::cout << "physInd = " << physInd << ", which is out of range." << std::endl ;
		exit(0) ;
	}
	return physInd ;
}

void Correlator::derivative(double *der, const int* eleNum)
{
	int physInd ;
	int idx = 0 ;
	int base = 1 ;
	for ( int i = 0; i < _site.size(); i ++ ) {
		physInd = getPhysInd(eleNum, i) ;
		idx += physInd * base ;
		base *= _siteDim ;
	}
	der[idx] += 1.0 / _elem[idx] ;
}

void Correlator::derivative(double &der_val, int &der_ind, const int* eleNum)
{
	int physInd ;
	int idx = 0 ;
	int base = 1 ;
	for ( int i = 0; i < _site.size(); i ++ ) {
		physInd = getPhysInd(eleNum, i) ;
		idx += physInd * base ;
		base *= _siteDim ;
	}
	der_val = 1.0 / _elem[idx] ;
	der_ind = idx ;
}

void Correlator::rescale(double maxElem)
{
	if (_nelements == 0) return;
	double max_value = 0 ;
	for (int i = 0 ; i <_nelements; i ++) {
		if ( fabs(_elem[i]) > fabs(max_value) ) {
			max_value = _elem[i] ;
		}
	}
	xscal(_nelements, maxElem/max_value, _elem) ;
}

void Correlator::info()
{
	cout << "number of sites in Correlator: " << _site.size() << endl ;
	cout << "Correlator sites: " ;
	for (int i = 0; i < _site.size(); i ++) {
		cout << _site.at(i) << ", " ;
	}
	cout << endl ;
}

void Correlator::disp()
{
	for (int i = 0; i < _nelements; i ++) {
		cout << _elem[i] << ", " ;
	}
	cout << endl ;
}
