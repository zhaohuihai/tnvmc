/*
 * tree_node.h
 *
 *  Created on: 2016年2月26日
 *      Author: zhaohuihai
 */

#ifndef SRC_TREE_NODE_H_
#define SRC_TREE_NODE_H_

#include "blas_lapack.hpp"

class TreeNode {
private:
	bool _ti ; // whether it is a translated node
	int _siteDim ; // dim of physical index
	int _bondDim ; // dim of virtual bond
	int _nelements ;
	bool _isLeaf ; // whether it is a leaf node
	std::vector<int> _sites ; // sites defining the leaf node
	int _nsites ; // number of sites in the lattice
	std::vector<int> _ti_sites ; // if a translated image, the sites defining the image's parent
	int _offset ; // positions of the Node variables in the ordering of all FTTN variables
	double* _elem ; // the pointer to the first element of this node
	std::vector<int> _dims ; // the dimensions of node tensor

	// node tensor indices order [parent, children], node number -10 for undefined parent and child nodes
	int _parent ; // node number of parent node. parent == -1 for root node;
	std::vector<int> _children ; // node number of child nodes.

	// store environment of parent, left, right
//	std::vector<double> _parentEnv ;
//	std::vector<double> _leftEnv, _rightEnv ;

	int getPhysInd(const int* eleNum, const int i) ;
public:
	// default constructor
	TreeNode() : _ti(false), _siteDim(0), _bondDim(0), _nelements(0), _isLeaf(false),
							 _nsites(0), _offset(0), _elem(0), _parent(-10) {}
	// constructor for leaf node
	TreeNode(const int nsites, const std::vector<int>& sites, const int siteDim, const int bondDim,
					 double* elem, int offset) ;
	// leaf nodes have no children but have physical indices
	TreeNode(const int nsites, const std::vector<int>& sites, const int siteDim, const int bondDim,
					 int parent, double* elem, int offset) ;
	// constructor for Internal node (including root node with parent == -1)
	TreeNode(const int bondDim, double* elem, int offset, const int rank) ;
	TreeNode(const int bondDim, int parent, std::vector<int> children, double* elem, int offset) ;

	int nelements() const { return _nelements; }
	bool isLeaf() const { return _isLeaf ; }
	std::vector<double> get_leaf(const int* eleNum) ;
	std::vector<double> get_leaf(const std::vector<int>& eleNum) ;
//	void get_leaf(const std::vector<int>& eleNum, std::vector<double>& leaf) ;
	bool isIncluded(const std::vector<int>& sites) ;
	std::vector<int> sites() const { return _sites ; }
	int size() const { return _sites.size() ; }
	int offset() const { return _offset ; }
	double* elem() const { return _elem ; }

	std::vector<int> dims() const { return _dims; }
	int parent() const { return _parent ; }
	std::vector<int> children() const { return _children ; }

	void assign(int dimFile, std::vector<double>& nodeFile) ;
	std::vector<double> get_tensor() ;
//	void set_parentEnv(const std::vector<double>& parentEnv ) { _parentEnv = parentEnv ; }
//	void set_leftEnv(const std::vector<double>& leftEnv) { _leftEnv = leftEnv ; }
//	void set_rightEnv(const std::vector<double>& rightEnv) { _rightEnv = rightEnv ; }
//	std::vector<double> get_parentEnv() const { return _parentEnv ; }
//	std::vector<double> get_leftEnv() const { return _leftEnv ; }
//	std::vector<double> get_rightEnv() const { return _rightEnv ; }

	std::vector<double> contractLeaves(std::vector<double>& lf0, std::vector<double>& lf1) ;
	std::vector<double> contractLeaves(std::vector<double>& lf0, std::vector<double>& lf1,
																	   std::vector<double>& buff) ;
	void contractLeaves(std::vector<double>& lf0,    std::vector<double>& lf1,
										  std::vector<double>& branch, std::vector<double>& buff) ;
	double contractRoot(std::vector<double>& lf28, std::vector<double>& lf29) ;

	void rescale(double maxElem) ;
	void info() ;
	void disp() ;
};

#endif /* SRC_TREE_NODE_H_ */
