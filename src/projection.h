/*
 * projection.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_PROJECTION_H_
#define SRC_PROJECTION_H_

#include "vmcmain.h"
#include <vector>

inline double ProjVal(const int *projCnt);
inline double LogProjVal(const int *projCnt);
inline double LogProjRatio(const int *projCntNew, const int *projCntOld);
inline double ProjRatio(const int *projCntNew, const int *projCntOld);
void MakeProjCnt(int *projCnt, const int *eleNum);
double computeLogProj(const int *eleNum) ;
double computeProj(const int *eleNum) ;
void UpdateProjCnt(const int ri, const int rj, const int s,
                   int *projCntNew, const int *projCntOld,
                   const int *eleNum);
std::vector<int> createSftEleNum(const int* eleNum,const int sft_x, const int sft_y) ;

inline double ProjVal(const int *projCnt) {
	int idx ;
	double z = 0.0 ;
	for(idx=0;idx<NProj;idx++) {
		z += Proj[idx] * (double)(projCnt[idx]);
	}
	return exp(z) ;
}

inline double LogProjVal(const int *projCnt) {
  int idx;
  double z=0;
  for(idx=0;idx<NProj;idx++) {
    z += Proj[idx] * (double)(projCnt[idx]);
  }
  return z;
}

inline double LogProjRatio(const int *projCntNew, const int *projCntOld) {
  int idx;
  double z=0;
  for(idx=0;idx<NProj;idx++) {
    z += Proj[idx] * (double)(projCntNew[idx]-projCntOld[idx]);
  }
  return z;
}

inline double ProjRatio(const int *projCntNew, const int *projCntOld) {
  int idx;
  double z=0;
  for(idx=0;idx<NProj;idx++) {
    z += Proj[idx] * (double)(projCntNew[idx]-projCntOld[idx]);
  }
  return exp(z);
}


#endif /* SRC_PROJECTION_H_ */
