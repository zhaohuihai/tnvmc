/*
 * safempi.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SAFEMPI_H_
#define SAFEMPI_H_

void SafeMpiReduce(double *send, double *recv, int nData, MPI_Comm comm);
void SafeMpiAllReduce(double *send, double *recv, int nData, MPI_Comm comm);
void SafeMpiBcast(double *buff, int nData, MPI_Comm comm);
void SafeMpiBcastInt(int *buff, int nData, MPI_Comm comm);

#endif /* SRC_SAFEMPI_H_ */
