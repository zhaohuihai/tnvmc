/*-------------------------------------------------------------
 * Variational Monte Carlo
 * global variables
 *-------------------------------------------------------------
 * by Satoshi Morita
 *-------------------------------------------------------------*/

#ifndef _INCLUDE_GLOBAL
#define _INCLUDE_GLOBAL

#define D_FileNameMax 256

/***** definition *****/
extern char CDataFileHead[]; /* prefix of output files */
extern char CParaFileHead[D_FileNameMax]; /* prefix for optimized variational parameters */

extern int NVMCCalMode; /* calculation mode
                    0: optimization of variational paraneters,
                    1: calculation of expectation values */
extern int NLanczosMode; /* mode of the single Lanczos step
                     0: none, 1: only energy, 2: Green functions */

extern int NStoreO; /* choice of store O: 0-> normal other-> store  */

extern int NDataIdxStart; /* starting value of the file index */
extern int NDataQtySmp; /* the number of output files */

extern int Nsite; /* the number of sites */
extern int Ne;    /* the number of electrons with up spin */
extern int Nsize; /* the number of electrons = 2*Ne */
extern int Nsite2; /* 2*Nsite */

extern int NSPGaussLeg; /* the number of points for the Gauss-Legendre quadrature */
extern int NSPStot; /* S of Spin projection */
extern int NMPTrans; /* the number of quantum projection for translation and point group symmetry */
extern int NQPFull; /* the total number of quantum projection = NSPGaussLeg*NMPTrans*NQPTransOpt */
extern int NQPFix; /* for QPFixWeight NSPGaussLeg*NMPTranss */

extern int NSROptItrStep; /* the number of SR method steps */
extern int NSROptItrSmp; /* the number of SR method steps for calculation of average value */
extern int NSROptFixSmp; /* the number of SR method steps with fixed samples (1 is recommended) */

extern double DSROptRedCut; /* SR stabilizing factor for truncation of redundant directions */
extern double DSROptStaDel; /* SR stabilizing factor for diagonal element modification */
extern double DSROptSftErr; // shift with max((DSROptSftErr * Sdiag_SD - Sdiag), 0)
extern double DSROptIniStep ; // initial SR step width
extern double DSROptStepDt; //  step width of the SR method
extern double DSROptErrCut ; // relative error of S diag for cut

extern bool isSparseO ;
extern bool isSweep ;
extern bool isRescaleSROpt ;

extern bool useSRCG ;
extern int CGinitial ; // 0. initial x is 0; 1. initial x is energy gradient
extern int CGprecond ; // use preconditioner or not in CG
extern double cg_thresh ; //
extern int max_cg_iter  ;

extern double DSROptRedRat; // step reduce ratio
extern double DSROptRandLB; // initial lower bound of random ratio interval (DSROptRandLB, 1) of SR step
extern double DSROptLBRati; // lower bound increase ratio r = 1-(1-DSROptRandLB)*DSROptLBRati^i
//--------------------------------------
// tune step in SR
extern double InitSteWid ; // initial SR step width
extern double SteIncRat ; // step increase ration in tuning
extern int    maxSteInc ; // max number of step increase
extern double maxSteWid ;
extern double minSteWid ;
extern int    tuNoSteWid ; // number of tuning step width
//--------------------------------------
extern int NVMCWarmUp; /* Monte Carlo steps for warming up */
extern int NVMCIniterval; /* sampling interval [MCS] */
extern int NVMCSample; /* the number of samples */
extern int NExUpdatePath; /* update by exchange hopping  0: off, 1: on */

extern int RndSeed; /* seed for pseudorandom number generator */
extern int NSplitSize; /* the number of inner MPI processes */
 
/* total length of def array */
extern int NTotalDefInt, NTotalDefDouble;

/* zlocspin.def */
extern int NLocSpn; /* the number of local spin */
extern int *LocSpn; /* [Nsite] */
/* local spin flag  0: local spin, 1: itinerant electron */

/* for Hamiltonian */
extern int NTransfer;
extern int **Transfer; /* [NTransfer][3] */
extern double *ParaTransfer;

extern int NCoulombIntra;
extern int *CoulombIntra; /* [NCoulombIntra] */
extern double *ParaCoulombIntra;

extern int NCoulombInter;
extern int **CoulombInter; /* [NCoulombInter][2] */
extern double *ParaCoulombInter;

extern int NHundCoupling;
extern int **HundCoupling; /* [NHundCoupling][2] */
extern double *ParaHundCoupling;

extern int NPairHopping;
extern int **PairHopping; /* [NPairHopping][2] */
extern double *ParaPairHopping;

extern int NExchangeCoupling;
extern int **ExchangeCoupling; /* [NExchangeCoupling][2] */
extern double *ParaExchangeCoupling;

extern int NInterAll;
extern int **InterAll; /* [NInterAll][6] */
extern double *ParaInterAll;

/* for variational parameters */
extern int NGutzwillerIdx, *GutzwillerIdx; /* [Nsite] */
extern int NJastrowIdx, **JastrowIdx; /* [Nsite][Nsite] */
extern int NDoublonHolon2siteIdx, **DoublonHolon2siteIdx; /* DoublonHolon2siteIdx[idx][2*Nsite] */
extern int NDoublonHolon4siteIdx, **DoublonHolon4siteIdx; /* DoublonHolon4siteIdx[idx][4*Nsite] */
extern int NOrbitalIdx, **OrbitalIdx; /* [Nsite][Nsite] */
extern int **OrbitalSgn; /* OrbitalSgn[Nsite][Nsite] = +1 or -1 */
//extern int NMPS, *MPS ;

/* zqptransidx.def */
extern int NQPTrans, **QPTrans; /* [NQPTrans][Nsite] */
extern int **QPTransSgn; /* QPTransSgn[NQPTrans][NSite] = +1 or -1 */
extern double *ParaQPTrans;

/* zqpopttrans.def */
extern int NQPOptTrans, **QPOptTrans; /* [NQPOptTrans][Nsite] */
extern int **QPOptTransSgn; /* QPOptTransSgn[NQPOptTrans][NSite] = +1 or -1 */
extern double *ParaQPOptTrans;

/* for Green functions */
extern int NCisAjs,         **CisAjsIdx;         /* [NCisAjs][3] */
extern int NCisAjsCktAlt,   **CisAjsCktAltIdx;   /* [NCisAjsCktAlt][8] */
extern int NCisAjsCktAltDC, **CisAjsCktAltDCIdx; /* [NCisAjsCktAltDC][6] */

/* Optimization flag */
extern int *OptFlag; /* [NPara]  1: optimized, 0 or 2: fixed */
extern bool OptProj ;
extern bool OptSlater ;
extern bool OptOptTrans ;
extern bool OptTNS ;

/* flag for anti-periodic boundry condition */
extern int APFlag; /* 0: periodic, 1: anti-periodic */

/* flag for shift of correlation factors */
/* 0: no shift, 1: shift. Set in ReadDefFileIdxPara(). */
extern int FlagShiftGJ;
extern int FlagShiftDH2;
extern int FlagShiftDH4;

/* flag for OptTrans mode */
extern int FlagOptTrans;
/* flag for Binary mode */
/* output zvo_var.dat (FileVar) as binary data */
extern int FlagBinary;

/* flag for file flush */
extern int NFileFlushInterval;

/***** Variational Parameters *****/
extern int NPara ;   /* the total number of variational prameters = NSlater + NProj + NOptTrans + NMPSelem */
extern int NProj;    /* the number of correlation factor */
extern int NSlater;  /* the number of pair orbital (f_ij) = NOrbitalIdx */
extern int NOptTrans;/* the number of weights for OptTrans. This is used only for variatonal parameters */
               /* NOptTrans = 0 (not OptTrans mode) or NQPOptTrans (OptTrans mode) */
extern int NPEPSelem ; // the number of elements in PEPS

extern int  initTNStype ;
extern double randShift ;

extern double *Para;   /* variatonal parameters */
extern double *Proj;   /* correlation factor     (Proj    =Para) */
extern double *Slater; /* pair orbital           (Slater  =Para+NProj) */
extern double *OptTrans; /* weights              (OptTrans=Para+NProj+NSlater) */
extern double *PEPSelem ; /* PEPS                (PEPSelem=Para+NProj+NSlater+NOptTrans) */
extern double *ParaChange ; //

extern int NmixPara ;
extern double *storePara ; //  storePara[NPara][NmixPara]

extern double PEPSampMax ;

/***** Electron Configuration ******/
extern int *EleIdx; /* EleIdx[sample][mi+si*Ne] */
extern int *EleCfg; /* EleCfg[sample][ri+si*Nsite] */
extern int *EleNum; /* EleIdx[sample][ri+si*Nsite] */
extern int *EleProjCnt; /* EleProjCnt[sample][proj] */
extern double *logSqPfFullSlater; /* logSqPfFullSlater[sample] */

extern int *TmpEleIdx;
extern int *TmpEleCfg;
extern int *TmpEleNum;
extern int *TmpEleProjCnt;

extern int *BurnEleIdx;
extern int *BurnEleCfg;
extern int *BurnEleNum;
extern int *BurnEleProjCnt;
extern int BurnFlag; /* 0: off, 1: on */

/***** Slater Elements ******/
extern double *SlaterElm; /* SlaterElm[QPidx][ri+si*Nsite][rj+sj*Nsite] */

extern double *InvM; /* InvM[QPidx][mi+si*Ne][mj+sj*Ne] */
extern double *PfM; /* PfM[QPidx] */
extern double *M ; // M[QPidx][Nsize*Nsize]
extern double *McondNum ; // McondNum[QPidx]
extern double *Mcol_sum ; // Mcol_sum[QPidx][Nsize] the column summation of M

/***** Quantum Projection *****/
extern double *QPFullWeight; /* QPFullWeight[NQPFull] */
extern double *QPFixWeight; /* QPFixWeight[NQPFix] */
extern double *SPGLCos, *SPGLSin; /* [NSPGaussLeg]  cos(beta/2) and sin(beta/2) */
extern double *SPGLCosSin, *SPGLCosCos, *SPGLSinSin; /* [NSPGaussLeg] */

/***** Tensor Network State *****/
extern char SBSpattern ;
extern bool diagMPS ;
extern bool usePairProd ; // whether to use pair product as reference
extern bool latticeCorr ; // square cps
extern bool allPairCorr ; // pair cps
extern bool absorbCorr ; // absorb smaller correlators into

extern int FTTN_NleafSites ;
extern int FTTN_inNodeRank ; // rank of internal nodes
extern int TTN_site_ratio ; // ratio of TTN No. and site No.

extern int length_x ;    // x direction length of lattice
extern int length_y ;    // y direction length of lattice
extern int unitCell_x ;  // x direction length of unit cell
extern int unitCell_y ;  // y direction legnth of unit cell
extern int latSymProj_x ; // x direction of lattice symmetry projection
extern int latSymProj_y ; // y direction of lattice symmetry projection
extern int rotSymProj ; // rotational symmetry projection
extern int corr_x ;
extern int corr_y ;
extern int NSROptTensor ; // number of tensors optimized at each step
extern int siteDim ;     /* physical dimension of every site */
extern int bondDim ;     /* bond dimension of every site tensor */
// 0: MPS
// 1: SBS
// 2: PEPS
// 3: fPEPS
// 4: CPS
// 5: FTTN: fat tree tensor network
// 6: TTNP: tree tensor network product
// 7: TTN: tree tensor network
#define TNStype 5
#define _mpi_use 1
extern int dimEven ;     // even parity dim
extern int chi    ;      // truncation dimension
extern double tol ;      // tolerance in canonical condition
extern int maxIt  ;      // max number of iteration

/***** Stocastic Reconfiguration *****/
extern int SROptSize; /* 1+NPara */
extern int SROptSize_sparse ;
extern double *SROptOO; /* [SROptSize*SROptSize] <O^\dagger O> */
extern double *SROptHO; /* [SROptSize]            < HO > */
extern double *SROptHO_SD ; // [SROptSize] standard deviation of <HO>
extern double *SROptO;  // [SROptSize] calculation buffer <O>
extern double *SROptO_SD ; //  [SROptSize] standard deviation of <O>
extern double *SROptOOdiag ; // [SROptSize] diagonal elements of <OO>
extern double *SROptOOdiag_SD ; // [SROptSize] standard deviation of diagonal elements of <OO>
extern double *SROptOOOdiag ; // [SROptSize] diagonal elements of <OOO>
extern double *SROptOOOOdiag ; // [SROptSize] diagonal elements of <OOOO>
extern double *SROptO_Store;  /* [SROptSize*NVMCSample] calculation buffer */
extern int    *SROptO_ind ; // index for sparse SROptO_Store

extern double *SROptData; /* [2+NPara] storage for energy and variational parameters */

/***** Physical Quantity *****/
extern double Wc; /* Weight for correlation sampling = <psi|x> */
extern double Etot; /* <H> */
extern double Etot2; /* <H^2> */
extern double Etot1 ;

extern double *PhysCisAjs; /* [NCisAjs] */
extern double *PhysCisAjsCktAlt; /* [NCisAjsCktAlt] */
extern double *PhysCisAjsCktAltDC; /* [NCisAjsCktAltDC] */
extern double *LocalCisAjs; /* [NCisAjs] */

extern const int NLSHam; /* 0: I, 1: H */
extern double *QQQQ; /* QQQQ[NLSHam][NLSHam][NLSHam][NLSHam]*/
extern double *LSLQ; /* [NLSHam][NLSHam]*/

extern double *QCisAjsQ; /* QCisAjsQ[NLSHam][NLSHam][NCisAjs]*/
extern double *QCisAjsCktAltQ; /* QCisAjsCktAltQ[NLSHam][NLSHam][NCisAjsCktAlt]*/
extern double *LSLCisAjs; /* [NLSHam][NCisAjs]*/

/***** Output File *****/
/* FILE *FileCfg; */
extern FILE *FileOut;
extern FILE *FileVar;
extern FILE *FileTime;
extern FILE *FileSRinfo; /* zvo_SRinfo.dat */
extern FILE *FileCisAjs;
extern FILE *FileCisAjsCktAlt;
extern FILE *FileCisAjsCktAltDC;
extern FILE *FileLS;
extern FILE *FileLSQQQQ;
extern FILE *FileLSQCisAjsQ;
extern FILE *FileLSQCisAjsCktAltQ;

/* FILE *FileTimerList; */
/* FILE *FileOpt;    /\* zqp_opt *\/ */

/***** HitachiTimer *****/
extern const int NTimer;
extern double Timer[], TimerStart[];

/***** openMP *****/
extern int NThread;

/***** for DGETRI and DSKPFA in CalculateMAll *****/
extern int LapackLWork;

/***** counter for vmcMake *****/
extern int Counter[];
/* 0: hopping, 1: hopping accept, 2: exchange try, 3: exchange accept */

extern bool output_energy ;
extern bool output_Smat ;
extern bool output_CGerr ;

extern int PfRecal ;
extern double Pf_thresh ;
extern double condNum_thresh ;

#endif /*  _INCLUDE_GLOBAL */
