/*-------------------------------------------------------------
 * Variational Monte Carlo
 * main program header
 *-------------------------------------------------------------
 * by Satoshi Morita and Ryui Kaneko
 *-------------------------------------------------------------*/

#ifndef _VMC_INCLUDE_FILES
#define _VMC_INCLUDE_FILES

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>
#include <string.h>
#include <cstddef>
#include <iostream>
#include <iomanip>

#include <omp.h>

//#ifdef _mpi_use
//  #include <mpi.h>
//#else
//typedef int MPI_Comm;
//MPI_Comm MPI_COMM_WORLD=0;
//inline void MPI_Init(int argc, char* argv[]) {return;}
//inline void MPI_Finalize() {return;}
//inline void MPI_Abort(MPI_Comm comm, int errorcode) {exit(errorcode); return;}
//inline void MPI_Barrier(MPI_Comm comm) {return;}
//inline void MPI_Comm_size(MPI_Comm comm, int *size) {*size = 1; return;}
//inline void MPI_Comm_rank(MPI_Comm comm, int *rank) {*rank = 0; return;}
//#endif /* _mpi_use */

#include <mpi.h>
//extern int omp_get_max_threads(void);
//extern int omp_get_thread_num(void);

//#include "sfmt/SFMT.h"
#include "global.h"

#include "blas_lapack.hpp"
//#include "stcopt_dposv.h"

//#ifdef HAVE_MKL
// #include <mkl.h>
//#endif
//#include "mkl.h"
//#include "fermionic_projected_entangled_pair_state.h"

extern "C" {
	void omp_set_num_threads(int) ;
  int omp_get_max_threads();
}

#endif /* _VMC_INCLUDE_FILES */
