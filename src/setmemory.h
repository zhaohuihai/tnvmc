/*
 * setmemory.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_SETMEMORY_H_
#define SRC_SETMEMORY_H_

void SetMemoryDef();
void FreeMemoryDef();
void SetMemory();
void FreeMemory();

#endif /* SRC_SETMEMORY_H_ */
