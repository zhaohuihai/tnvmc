/*
 * lslocgrn.h
 *
 *  Created on: 2015年3月22日
 *      Author: zhaohuihai
 */

#ifndef SRC_LSLOCGRN_H_
#define SRC_LSLOCGRN_H_

#include "fat_tree_tensor_network.h"

void LSLocalQ(const double h1, const double ip, int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt);

void LSLocalQ(const double h1, const double ip, int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt,
		          FatTreeTensorNetwork& fttn);

void LSLocalCisAjs(const double h1, const double ip, int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt);
void LSLocalCisAjs(const double h1, const double ip, int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt,
									 FatTreeTensorNetwork& fttn);

double calculateHK(const double h1, const double ip, int *eleIdx, int *eleCfg,
                   int *eleNum, int *eleProjCnt);
double calculateHK(const double h1, const double ip, int *eleIdx, int *eleCfg,
                   int *eleNum, int *eleProjCnt, FatTreeTensorNetwork& fttn);
double calculateHW(const double h1, const double ip, int *eleIdx, int *eleCfg,
                   int *eleNum, int *eleProjCnt);
double calculateHW(const double h1, const double ip, int *eleIdx, int *eleCfg,
                   int *eleNum, int *eleProjCnt, FatTreeTensorNetwork& fttn);

double calHCA(const int ri, const int rj, const int s,
              const double h1, const double ip, int *eleIdx, int *eleCfg,
              int *eleNum, int *eleProjCnt);
double calHCA(const int ri, const int rj, const int s,
              const double h1, const double ip, int *eleIdx, int *eleCfg,
              int *eleNum, int *eleProjCnt, FatTreeTensorNetwork& fttn);
double calHCACA(const int ri, const int rj, const int rk, const int rl,
                const int si,const int sk,
                const double h1, const double ip, int *eleIdx, int *eleCfg,
                int *eleNum, int *eleProjCnt);
double calHCACA(const int ri, const int rj, const int rk, const int rl,
                const int si,const int sk,
                const double h1, const double ip, int *eleIdx, int *eleCfg,
                int *eleNum, int *eleProjCnt, FatTreeTensorNetwork& fttn);

double checkGF1(const int ri, const int rj, const int s, const double ip,
                int *eleIdx, const int *eleCfg, int *eleNum);
double calHCA1(const int ri, const int rj, const int s,
               const double ip, int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt);
double calHCA1(const int ri, const int rj, const int s,
               const double ip, int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt,
							 FatTreeTensorNetwork& fttn);
double calHCA2(const int ri, const int rj, const int s,
               const double ip, int *eleIdx, int *eleCfg, int *eleNum,
							 int *eleProjCnt);
double calHCA2(const int ri, const int rj, const int s,
               const double ip, int *eleIdx, int *eleCfg, int *eleNum,
							 int *eleProjCnt, FatTreeTensorNetwork& fttn);

double checkGF2(const int ri, const int rj, const int rk, const int rl,
                const int s, const int t, const double ip,
                int *eleIdx, const int *eleCfg, int *eleNum);
double calHCACA1(const int ri, const int rj, const int rk, const int rl,
                 const int si,const int sk,
                 const double ip, int *eleIdx, int *eleCfg,
                 int *eleNum, int *eleProjCnt);
double calHCACA1(const int ri, const int rj, const int rk, const int rl,
                 const int si,const int sk,
                 const double ip, int *eleIdx, int *eleCfg, int *eleNum,
								 int *eleProjCnt, FatTreeTensorNetwork& fttn);
double calHCACA2(const int ri, const int rj, const int rk, const int rl,
                 const int si,const int sk,
                 const double ip, int *eleIdx, int *eleCfg, int *eleNum, int *eleProjCnt);
double calHCACA2(const int ri, const int rj, const int rk, const int rl,
                 const int si,const int sk,
                 const double ip, int *eleIdx, int *eleCfg, int *eleNum,
								 int *eleProjCnt, FatTreeTensorNetwork& fttn);

void copyMAll(double *invM_from, double *pfM_from, double *invM_to, double *pfM_to);

#endif /* SRC_LSLOCGRN_H_ */
