#!/bin/bash

length_x=4
length_y=4
unitCell_x=4 
unitCell_y=4
Ne_up=5
Hubbard_U=10.0

tp=0.0
AF=0.0
SC=0.0
APFlag=0

python makeDef_Hubbard2dAPSCAft.py ${length_x} ${length_y} ${unitCell_x} ${unitCell_x} ${Ne_up} ${Hubbard_U}
perl -w trans.prl
perl -w U_intra.prl
perl -w U_inter.prl
perl -w J_inter.prl
perl -w local.prl
perl -w gutzw.prl
#perl -w no_2doublon_holon.prl
#perl -w no_4doublon_holon.prl
perl -w full_jastr.prl
#perl -w full_qptrans.prl
perl -w full_norbital.prl
#perl -w CisAjs.prl
#perl -w CisAjsCktAlt.prl
#perl -w CisAjsCktAltDC.prl
python initAFSC.py ${length_x} ${length_y} ${Ne_up} ${tp} ${AF} ${SC} ${APFlag}
