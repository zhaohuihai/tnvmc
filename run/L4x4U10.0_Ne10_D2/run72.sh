#!/bin/sh
#QSUB -queue F144cpu
#QSUB -node 72
#QSUB -mpi 1728
#QSUB -omp 1
#QSUB -place pack
#QSUB -over false
#PBS -l walltime=01:00:00
#PBS -N D2

#mpijob ../../bin/tnvmc.exe xnamelist.def VMC_zinitial.def > output 2>&1
#mpijob ../../bin/tnvmc.exe xnamelist.def D0para.dat > output 2>&1
mpijob ../../bin/tnvmc.exe xnamelist.def D2para.dat > output 2>&1
#mpijob ../../bin/tnvmc.exe xnamelist.def zqp_opt.dat > output 2>&1
#mpijob ../../bin/tnvmc.exe xnamelist.def zvo_var_001.dat > output 2>&1

