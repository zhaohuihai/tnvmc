#!/bin/sh
#QSUB -queue F4cpu
#QSUB -node 4
#QSUB -mpi 96
#QSUB -omp 1
#QSUB -place pack
#QSUB -over false
#PBS -l walltime=4:00:00
#PBS -N D2

#mpijob ../../bin/tnvmc.exe xnamelist.def VMC_zinitial.def > output 2>&1
#mpijob ../../bin/tnvmc.exe xnamelist.def D0para.dat > output 2>&1
#mpijob ../../bin/tnvmc.exe xnamelist.def zqp_init.dat > output 2>&1
#mpijob ../../bin/tnvmc.exe xnamelist.def zqp_opt.dat > output 2>&1
mpijob ../../bin/tnvmc.exe xnamelist.def zvo_var_001.dat > output 2>&1

