
include makerule.mk

# all source files directories
src_root_dir := src
src_dirs := $(shell find ./$(src_root_dir) -maxdepth 10 -type d)

# .cpp source files
src_files := $(foreach dir, $(src_dirs), $(wildcard $(dir)/*.cpp) )

# all object files directories
obj_root_dir := obj
obj_files = $(patsubst %.cpp, %.o, $(src_files))
objects := $(subst $(src_root_dir),$(obj_root_dir),$(obj_files))

TARGET = bin/tnvmc.exe
PFAPACK = lib/libpfapack.a

# when make is run with no arguments, create the executable
.PHONY : default vmcmain pfapack sfmt
default : $(TARGET)

# create the executable
$(TARGET) : vmcmain pfapack sfmt
	$(MPICC) $(objects) $(PFAPACK) $(CC_FLAGS) $(GLOBAL_LIB) -o $@;

vmcmain : 
	$(MAKE) -C src
pfapack :
	$(MAKE) -C src/pfapack
sfmt :
	$(MAKE) -C src/sfmt
	
# show
.PHONY : show
show:
	@echo "object files"
	@echo $(objects)


.PHONY : clean
clean :
	$(MAKE) -C src clean
	$(MAKE) -C src/pfapack clean
	$(MAKE) -C src/sfmt clean
